﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoadGame
{
    public static GameSaveData game_data = new GameSaveData();

    public static void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/save.game"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/save.game", FileMode.Open);
            SaveLoadGame.game_data = (GameSaveData)bf.Deserialize(file);
            file.Close();
        }
    }

    public static void SaveGame()
    {
        if (game_data != null)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/save.game");
            bf.Serialize(file, SaveLoadGame.game_data);
            file.Close();
        }
    }
}
