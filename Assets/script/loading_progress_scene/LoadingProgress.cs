﻿using UnityEngine;
using System.Collections;

public class LoadingProgress : MonoBehaviour
{
    public Texture2D empty_progressbar;
    public Texture2D full_progressbar;

    private AsyncOperation load_async = null;

    public IEnumerator LoadStageAsync(string level)
    {
        load_async = Application.LoadLevelAsync(level);
        yield return load_async;
    }

    void OnGUI()
    {
        if (load_async != null)
        {
            GUI.DrawTexture(new Rect(0, 0, 100, 50), empty_progressbar);
            GUI.DrawTexture(new Rect(0, 0, 100 * load_async.progress, 50), full_progressbar);
        }
    }
}
