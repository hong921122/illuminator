﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChapterMouseOver : MonoBehaviour
{
    void OnMouseEnter()
    {
        if (gameObject.GetComponent<Animation>() == null)
        {
            SpriteRenderer sprite = this.GetComponent<SpriteRenderer>();
            sprite.color = new Color(255, 0, 0);
        }
    }

    void OnMouseExit()
    {
        SpriteRenderer sprite = this.GetComponent<SpriteRenderer>();
        sprite.color = new Color(255, 255, 255);
    }

    void OnMouseDown()
    {
        if (gameObject.name == "back")
        {
            StartCoroutine("SelectMenu", "initial_scene");
        }
        if (gameObject.name == "arrow_left" || gameObject.name == "arrow_right")
        {
            List<string> chapter_names = Camera.main.GetComponent<ChapterSceneController>().chapter_names;
            int chapter_index = Camera.main.GetComponent<ChapterSceneController>().chapter_index;
            if (gameObject.name == "arrow_left")
            {
                if (chapter_index != 0)
                {
                    GameObject.Find(chapter_names[chapter_index]).GetComponent<Collider2D>().enabled = false;
                    GameObject.Find(chapter_names[chapter_index]).GetComponent<ChapterChangeAnimation>().animationPlay(false, true);
                    GameObject.Find(chapter_names[chapter_index - 1]).GetComponent<ChapterChangeAnimation>().animationPlay(false, false);
                    Camera.main.GetComponent<ChapterSceneController>().chapter_index = (chapter_index - 1);
                    Camera.main.GetComponent<ChapterSceneController>().current_chapter_name = chapter_names[chapter_index - 1];
                }
            }
            if (gameObject.name == "arrow_right")
            {
                if (chapter_index != (chapter_names.Count - 1))
                {
                    GameObject.Find(chapter_names[chapter_index]).GetComponent<ChapterChangeAnimation>().animationPlay(true, true);
                    GameObject.Find(chapter_names[chapter_index + 1]).GetComponent<ChapterChangeAnimation>().animationPlay(true, false);
                    Camera.main.GetComponent<ChapterSceneController>().chapter_index = (chapter_index + 1);
                    Camera.main.GetComponent<ChapterSceneController>().current_chapter_name = chapter_names[chapter_index + 1];
                }
            }
        }
        if (gameObject.name == "Desert")
        {
            StartCoroutine("SelectMenuFadeWhite", "chapter_1");
            MusicManager.instance.Stop();
        }
    }

    private IEnumerator SelectMenu(string level)
    {
        yield return new WaitForSeconds(0.6f);
        float fade_time = GameObject.Find("Main Camera").GetComponent<FadeScene>().BeginFade(1, true);
        yield return new WaitForSeconds(fade_time);
        Application.LoadLevel(level);
    }

    private IEnumerator SelectMenuFadeWhite(string level)
    {
        yield return new WaitForSeconds(0.6f);
        GameObject.Find("Main Camera").GetComponent<FadeScene>().fade_speed = 3.0f;
        float fade_time = GameObject.Find("Main Camera").GetComponent<FadeScene>().BeginFade(1, false);
        yield return new WaitForSeconds(fade_time);
        Application.LoadLevel(level);
    }
}
