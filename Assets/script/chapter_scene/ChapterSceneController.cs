﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChapterSceneController : MonoBehaviour
{
    [HideInInspector]
    public List<string> chapter_names;
    [HideInInspector]
    public string current_chapter_name;
    [HideInInspector]
    public int chapter_index;

    void Awake()
    {
        LevelMenu2D.I.OnItemClicked += I_OnItemClicked;
    }

    void I_OnItemClicked(int itemIndex, GameObject itemObject)
    {
        //LoadLevel
        Debug.Log(itemObject.name);
    }

    // Use this for initialization
    void Start()
    {
        Transform tr = GameObject.Find("New Sprite").transform;
        SpriteRenderer sr = GameObject.Find("New Sprite").GetComponent<SpriteRenderer>();

        tr.transform.localScale = new Vector3(1, 1, 1);

        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;

        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        tr.transform.localScale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, tr.transform.localScale.z);

        chapter_names = new List<string>(new string[] { "Desert", "Factory", "Temple", "Forest" });
        chapter_index = 0;
        current_chapter_name = "Desert";

        //foreach (string chapter in chapter_names)
        //{
        //    if (chapter != current_chapter_name)
        //    {
        //        Transform pos = GameObject.Find(chapter).transform;
        //        pos.localPosition = new Vector3(-20.0f, 0.0f, 1);
        //        SpriteRenderer sprite = GameObject.Find(chapter).GetComponent<SpriteRenderer>();
        //        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0);
        //    }
        //}
        //GameObject.Find("background_image").GetComponent<GUITexture>().pixelInset = new Rect(Screen.width / 2, Screen.height / 2, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mouse_pos = Input.mousePosition;
        mouse_pos.z = 3.0f;
        GameObject.Find("Point light").transform.position = Camera.main.ScreenToWorldPoint(mouse_pos);
    }

    void OnGUI()
    {

    }
}
