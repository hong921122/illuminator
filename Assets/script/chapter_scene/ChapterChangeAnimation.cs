﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class ChapterChangeAnimation : MonoBehaviour
{
    private AnimationClip change_animationclip;

    /// <summary>
    /// </summary>
    /// </summary>
    /// <param name="direction_flag">If set to <c>true</c> arrow_right, If set to <c>false</c> arrow_left</param>
    /// <param name="current">if set to <c>true</c> this sprite is a current chapter
    public void animationPlay(bool direction_flag, bool current)
    {
        //alpha
        AnimationCurve curve;
        if (gameObject.GetComponent<SpriteRenderer>().color.a != 0)
            curve = AnimationCurve.Linear(0.0f, 1.0f, 0.1f, 0.0f);
        else
            curve = AnimationCurve.Linear(0.0f, 0.0f, 0.1f, 1.0f);

        AnimationCurve curve2;
        //x position
        if (!current)
        {
            curve2 = AnimationCurve.Linear(0.0f, transform.position.x, 1.0f, 0.0f);
        }
        else
        {
            if (direction_flag)
            {
                curve2 = AnimationCurve.Linear(0.0f, transform.position.x, 1.0f, transform.position.x + 20.0f);
            }
            else
            {
                curve2 = AnimationCurve.Linear(0.0f, transform.position.x, 1.0f, transform.position.x - 20.0f);
            }
        }

        //z position
        AnimationCurve curve3 = AnimationCurve.Linear(0.0f, 1.0f, 1.0f, 1.0f);
        change_animationclip = new AnimationClip();
        change_animationclip.SetCurve("", typeof(SpriteRenderer), "m_Color.a", curve);
        change_animationclip.SetCurve("", typeof(Transform), "localPosition.x", curve2);
        change_animationclip.SetCurve("", typeof(Transform), "localPosition.z", curve3);
        animation.AddClip(change_animationclip, "change_chapter");
        animation.Play("change_chapter");
    }
}
