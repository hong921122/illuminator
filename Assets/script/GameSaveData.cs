﻿using UnityEngine;
using System.Collections;
using System.IO;

[System.Serializable]
public class GameSaveData
{
    public int chapter_id { get; set; }

    // 1 = small
    // 2 = normal
    // 3 = big
    public int character_scalestate { get; set; }
	public int red_energy { get; set; }
	public int blue_energy { get; set; }
	public int green_energy { get; set; }
    public float savepoint_x { get; set; }
    public float savepoint_y { get; set; }
    public float savepoint_z { get; set; }

    public bool[] obj_color_energy;
    public bool[] obj_cascade;
    public bool[] obj_moving_ground;
    public bool[] obj_door;
    public bool[][] obj_lightwall;
    public float[] obj_spotlight_angle;
    public float[] obj_mirror_angle;

    public GameSaveData(int chapter, GameObject save_object)
    {
        this.chapter_id = chapter;
    }

    public GameSaveData()
    {
    }
}
