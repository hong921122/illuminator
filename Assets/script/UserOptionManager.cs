﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;

//mouse sensitivity
//sound
//keyboard
public class UserOptionManager : MonoBehaviour
{
    private static UserOptionManager _instance;
    private KeyCode _MOVE_RIGHT;
    private KeyCode _MOVE_LEFT;
    private KeyCode _MOVE_UP;
    private KeyCode _MOVE_DOWN;
    private KeyCode _JUMP;
    private KeyCode _USE_OBJECT;
    private KeyCode _STOP_OBJECT;
    private KeyCode _SPHERE_COLOR_TYPE_1;
    private KeyCode _SPHERE_COLOR_TYPE_2;
    private KeyCode _SPHERE_COLOR_TYPE_3;
    private KeyCode _SPHERE_COLOR_TYPE_4;
    private KeyCode _RESET;
    private float _MOUSE_SENSITIVITY;
    private float _VOLUME_BGM;
    private float _VOLUME_EFFECT;

    public static UserOptionManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UserOptionManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    public KeyCode MOVE_RIGHT
    {
        get
        {
            _MOVE_RIGHT = (KeyCode)PlayerPrefs.GetInt("MOVE_RIGHT", Convert.ToInt32(KeyCode.D));
            return _MOVE_RIGHT;
        }
        set
        {
            PlayerPrefs.SetInt("MOVE_RIGHT", Convert.ToInt32(value));
            _MOVE_RIGHT = value;
        }
    }

    public KeyCode MOVE_LEFT
    {
        get
        {
            _MOVE_LEFT = (KeyCode)PlayerPrefs.GetInt("MOVE_LEFT", Convert.ToInt32(KeyCode.A));
            return _MOVE_LEFT;
        }
        set
        {
            PlayerPrefs.SetInt("MOVE_LEFT", Convert.ToInt32(value));
            _MOVE_LEFT = value;
        }
    }

    public KeyCode MOVE_UP
    {
        get
        {
            _MOVE_UP = (KeyCode)PlayerPrefs.GetInt("MOVE_UP", Convert.ToInt32(KeyCode.W));
            return _MOVE_UP;
        }
        set
        {
            PlayerPrefs.SetInt("MOVE_UP", Convert.ToInt32(value));
            _MOVE_UP = value;
        }
    }

    public KeyCode MOVE_DOWN
    {
        get
        {
            _MOVE_DOWN = (KeyCode)PlayerPrefs.GetInt("MOVE_DOWN", Convert.ToInt32(KeyCode.S));
            return _MOVE_DOWN;
        }
        set
        {
            PlayerPrefs.SetInt("MOVE_DOWN", Convert.ToInt32(value));
            _MOVE_DOWN = value;
        }
    }

    public KeyCode JUMP
    {
        get
        {
            _JUMP = (KeyCode)PlayerPrefs.GetInt("JUMP", Convert.ToInt32(KeyCode.Space));
            return _JUMP;
        }
        set
        {
            PlayerPrefs.SetInt("JUMP", Convert.ToInt32(value));
            _JUMP = value;
        }
    }

    public KeyCode USE_OBJECT
    {
        get
        {
            _USE_OBJECT = (KeyCode)PlayerPrefs.GetInt("USE_OBJECT", Convert.ToInt32(KeyCode.E));
            return _USE_OBJECT;
        }
        set
        {
            PlayerPrefs.SetInt("USE_OBJECT", Convert.ToInt32(value));
            _USE_OBJECT = value;
        }
    }

    public KeyCode STOP_OBJECT
    {
        get
        {
            _STOP_OBJECT = (KeyCode)PlayerPrefs.GetInt("STOP_OBJECT", Convert.ToInt32(KeyCode.Q));
            return _STOP_OBJECT;
        }
        set
        {
            PlayerPrefs.SetInt("STOP_OBJECT", Convert.ToInt32(value));
            _STOP_OBJECT = value;
        }
    }

    public KeyCode SPHERE_COLOR_TYPE_1
    {
        get
        {
            _SPHERE_COLOR_TYPE_1 = (KeyCode)PlayerPrefs.GetInt("SPHERE_COLOR_TYPE_1", Convert.ToInt32(KeyCode.Alpha1));
            return _SPHERE_COLOR_TYPE_1;
        }
        set
        {
            PlayerPrefs.SetInt("SPHERE_COLOR_TYPE_1", Convert.ToInt32(value));
            _SPHERE_COLOR_TYPE_1 = value;
        }
    }

    public KeyCode SPHERE_COLOR_TYPE_2
    {
        get
        {
            _SPHERE_COLOR_TYPE_2 = (KeyCode)PlayerPrefs.GetInt("SPHERE_COLOR_TYPE_2", Convert.ToInt32(KeyCode.Alpha2));
            return _SPHERE_COLOR_TYPE_2;
        }
        set
        {
            PlayerPrefs.SetInt("SPHERE_COLOR_TYPE_2", Convert.ToInt32(value));
            _SPHERE_COLOR_TYPE_2 = value;
        }
    }

    public KeyCode SPHERE_COLOR_TYPE_3
    {
        get
        {
            _SPHERE_COLOR_TYPE_3 = (KeyCode)PlayerPrefs.GetInt("SPHERE_COLOR_TYPE_3", Convert.ToInt32(KeyCode.Alpha3));
            return _SPHERE_COLOR_TYPE_3;
        }
        set
        {
            PlayerPrefs.SetInt("SPHERE_COLOR_TYPE_3", Convert.ToInt32(value));
            _SPHERE_COLOR_TYPE_3 = value;
        }
    }

    public KeyCode SPHERE_COLOR_TYPE_4
    {
        get
        {
            _SPHERE_COLOR_TYPE_4 = (KeyCode)PlayerPrefs.GetInt("SPHERE_COLOR_TYPE_4", Convert.ToInt32(KeyCode.Alpha4));
            return _SPHERE_COLOR_TYPE_4;
        }
        set
        {
            PlayerPrefs.SetInt("SPHERE_COLOR_TYPE_4", Convert.ToInt32(value));
            _SPHERE_COLOR_TYPE_4 = value;
        }
    }

    public KeyCode RESET
    {
        get
        {
            _RESET = (KeyCode)PlayerPrefs.GetInt("RESET", Convert.ToInt32(KeyCode.R));
            return _RESET;
        }
        set
        {
            PlayerPrefs.SetInt("RESET", Convert.ToInt32(value));
            _RESET = value;
        }
    }

    public float MOUSE_SENSITIVITY
    {
        get
        {
            _MOUSE_SENSITIVITY = PlayerPrefs.GetFloat("MOUSE_SENSITIVITY", 10);
            return _MOUSE_SENSITIVITY;
        }
        set
        {
            PlayerPrefs.SetFloat("MOUSE_SENSITIVITY", value);
            _MOUSE_SENSITIVITY = value;
        }
    }

    public float VOLUME_BGM
    {
        get
        {
            _VOLUME_BGM = PlayerPrefs.GetFloat("VOLUME_BGM", 0.8f);
            return _VOLUME_BGM;
        }
        set
        {
            PlayerPrefs.SetFloat("VOLUME_BGM", value);
            _VOLUME_BGM = value;
        }
    }

    public float VOLUME_EFFECT
    {
        get
        {
            _VOLUME_EFFECT = PlayerPrefs.GetFloat("VOLUME_EFFECT", 0.8f);
            return _VOLUME_EFFECT;
        }
        set
        {
            PlayerPrefs.SetFloat("VOLUME_EFFECT", value);
            _VOLUME_EFFECT = value;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if (this != _instance)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
