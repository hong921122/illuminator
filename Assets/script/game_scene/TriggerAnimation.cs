﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class TriggerAnimation : MonoBehaviour
{

    void Start()
    {
        AnimationCurve curve1 = AnimationCurve.EaseInOut(0.0f, transform.localScale.x, 0.5f, transform.localScale.x * 3.0f);
        AnimationCurve curve2 = AnimationCurve.EaseInOut(0.0f, transform.localScale.y, 0.5f, transform.localScale.y * 3.0f);

        AnimationClip clip = new AnimationClip();
        clip.SetCurve("", typeof(Transform), "localScale.x", curve1);
        clip.SetCurve("", typeof(Transform), "localScale.y", curve2);
        animation.AddClip(clip, "TriggerPopUp");
        animation["TriggerPopUp"].wrapMode = WrapMode.Once;
    }

    public void renderAnimation(bool popUp)
    {
        if (popUp)
        {
            animation.Play("TriggerPopUp");
        }
        else
        {
            transform.localScale = new Vector3(0.5f, 0.5f, 1.0f);
        }
    }
}
