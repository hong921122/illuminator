﻿using UnityEngine;
using System.Collections;

public enum ELightColor {
	WHITE, RED, GREEN, BLUE
}

public enum EBottomMaterial {
	WATER, SAND, METAL
}

public enum EInteractWith {
	NORMAL,
	HANGLAMP,
	STREETLAMP,
	SPOTLIGHT,
	SANDWIND
}

public enum EDirection {
	RIGHT, LEFT, UP, DOWN
}

public enum ESphereState{
	NORMAL, HOOK, CLICKED
}

public enum ELensType {
    CONCAVE, // 오목
    CONVEX  // 볼록
}

public enum ECharacterScaleState {
    NORMAL,
    BIG,
    SMALL
}

public enum ERotationDoorState {
	OPEN,
	COLSE
}