﻿using UnityEngine;
using System.Collections;

public class tutorialTrigger : MonoBehaviour
{
    // character flag for only
    private bool tutorialTriggerFlag = false;
    private SpriteRenderer keyImageRenderer;
    private PixelCrushers.DialogueSystem.AlertTrigger alert;

    public string tutorial_imgpath;

    // Use this for initialization
    void Start()
    {
        keyImageRenderer = GetComponentInChildren<SpriteRenderer>();
        alert = GetComponent<PixelCrushers.DialogueSystem.AlertTrigger>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!tutorialTriggerFlag && col.gameObject.tag == "character")
        {
            tutorialTriggerFlag = true;
            alert.enabled = true;

            //keyImageRenderer.sprite = Resources.Load<Sprite>("botton/key36");
            keyImageRenderer.sprite = Resources.Load<Sprite>(tutorial_imgpath);
            GetComponentInChildren<TriggerAnimation>().renderAnimation(true);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (tutorialTriggerFlag && col.gameObject.tag == "character")
        {
            tutorialTriggerFlag = false;
            alert.enabled = false;

            keyImageRenderer.sprite = Resources.Load<Sprite>("none");
            GetComponentInChildren<TriggerAnimation>().renderAnimation(false);
        }
    }
}
