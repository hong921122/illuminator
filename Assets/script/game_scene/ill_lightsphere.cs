﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ill_lightsphere : MonoBehaviour
{
    private ill_character character;

    //particle
    [HideInInspector]
    public ParticleEmitter particleEmitter;
    [HideInInspector]
    public ParticleAnimator particleAnimator;
    [HideInInspector]
    public ParticleRenderer particleRender;

	public List<Material> childMaterial;
	public ParticleSystem childParticleSystem;

    private GameObject current_hooked_object = null;

    [HideInInspector]
	public ESphereState sphereState = ESphereState.NORMAL;

    [HideInInspector]
    public ELightColor sphereColor;

    private ill_light_direct checker;

    private Vector2 clicked_pos;

    SliderJoint2D sliderjoint;
    SpringJoint2D springjoint;
    DistanceJoint2D distancejoint;

    private float last_distance;
    private Vector2 shoot_pos;  // 발사했을 때 sphere의 위치

    public Material white;
    public Material red;
    public Material green;
    public Material blue;

    // Use this for initialization
    void Start()
    {
        character = GameManager.character;
		childParticleSystem = this.gameObject.GetComponentInChildren<ParticleSystem>();

		childMaterial = new List<Material> ();
        //childMaterial.Add(Resources.LoadAssetAtPath<Material>("Assets/Material/sphere_material_white.mat"));
        //childMaterial.Add(Resources.LoadAssetAtPath<Material>("Assets/Material/sphere_material_red.mat"));
        //childMaterial.Add(Resources.LoadAssetAtPath<Material>("Assets/Material/sphere_material_green.mat"));
        //childMaterial.Add(Resources.LoadAssetAtPath<Material>("Assets/Material/sphere_material_blue.mat"));
        childMaterial.Add(white);
        childMaterial.Add(red);
        childMaterial.Add(green);
        childMaterial.Add(blue);

        // default white color
        this.childParticleSystem.renderer.material = childMaterial[0];

        //particle
        particleRender = gameObject.GetComponent<ParticleRenderer>();
        particleEmitter = gameObject.GetComponent<ParticleEmitter>();
        particleAnimator = gameObject.GetComponent<ParticleAnimator>();

        sliderjoint = this.gameObject.GetComponent<SliderJoint2D>();
        springjoint = this.gameObject.GetComponent<SpringJoint2D>();
        distancejoint = this.gameObject.GetComponent<DistanceJoint2D>();

        springjoint.dampingRatio = 1f;
        distancejoint.distance = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        if (sphereState == ESphereState.HOOK)
        {
            //this.rigidbody2D.velocity = (current_hooked_object.transform.position - transform.position) * 5;
			this.transform.position = current_hooked_object.transform.position;
            gameObject.GetComponent<SpringJoint2D>().enabled = false;
        }
        else
        {
            current_hooked_object = null;
            gameObject.GetComponent<SpringJoint2D>().enabled = true;
        }



        // if light state normal, stay near character
        if (sphereState == ESphereState.NORMAL
            || sphereState == ESphereState.CLICKED)
        {

            Vector2 new_position;
            //if(character.transform.localScale.x < 0)
            //    new_position = new Vector2(character.transform.position.x + 1.5f, character.transform.position.y + 2.5f);
            //else 
            //    new_position = new Vector2(character.transform.position.x - 1.5f, character.transform.position.y + 2.5f);
            new_position = new Vector2(character.transform.position.x, character.transform.position.y + 3.7f);


            springjoint.connectedAnchor = new_position;
            sliderjoint.connectedAnchor = new_position;
            sliderjoint.angle = Vector2.Angle(Vector2.right, clicked_pos - new_position);
            if (clicked_pos.y < new_position.y)
                sliderjoint.angle *= -1;


            distancejoint.connectedAnchor = new_position;
            distancejoint.maxDistanceOnly = true;


            if (last_distance > (shoot_pos - (Vector2)this.transform.position).magnitude)
            {
                sphereState = ESphereState.NORMAL;
            }

            last_distance = (shoot_pos - (Vector2)this.transform.position).magnitude;
        }


        DrawParticleCharacterToSphere(character.transform.position, transform.position);

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (sphereState == ESphereState.CLICKED || sphereState == ESphereState.HOOK)
        {
            // testing for light of streetlamp
            if (col != null && col.gameObject.name == "light" && col.gameObject != current_hooked_object)
            {

                checker = col.gameObject.GetComponent<ill_light_direct>();
                if (checker.lightColor == sphereColor && character.getEnergyCount(checker.lightColor) > 0)
                {
                    character.decEnergyCount(checker.lightColor);

                    sphereState = ESphereState.HOOK;

                    current_hooked_object = col.gameObject;
                    character.interacting_object = col.gameObject;



                    switch (col.tag)
                    {
                        case "streetlamp":
                            Vector2 velocity = transform.position - character.transform.position;
                            velocity.Normalize();
                            velocity *= 7.0f;

                            character.rigidbody2D.velocity = velocity;
                            character.interacting_type = EInteractWith.STREETLAMP;
                            break;

                        case "hanglamp":
                            if (character.scale_state != ECharacterScaleState.BIG)
                            {
                                character.interacting_type = EInteractWith.HANGLAMP;
								this.transform.SetParent(col.gameObject.transform);
                            }

                            break;
                    }
                }
            }
        }
    }

    private void DrawParticleCharacterToSphere(Vector3 character_pos, Vector3 sphere_pos)
    {
        Color[] modifiedColors = particleAnimator.colorAnimation;
        if (sphereColor == ELightColor.WHITE)
        {
            for (int i = 0; i < 5; i++) { modifiedColors[i] = Color.white; }
        }
        else if (sphereColor == ELightColor.RED)
        {
            for (int i = 0; i < 5; i++) { modifiedColors[i] = Color.red; }
        }
        else if (sphereColor == ELightColor.GREEN)
        {
            for (int i = 0; i < 5; i++) { modifiedColors[i] = Color.green; }
        }
        else if (sphereColor == ELightColor.BLUE)
        {
            for (int i = 0; i < 5; i++) { modifiedColors[i] = Color.blue; }
        }
        particleAnimator.colorAnimation = modifiedColors;
        particleEmitter.localVelocity = (character.transform.position - transform.position + new Vector3(0.0f, 2.3f, 0.0f)) * 0.5f;
    }

    public void MoveSphere(Vector2 mouse_pos)
    {

        clicked_pos = Camera.main.ScreenToWorldPoint(mouse_pos);
        Vector2 sphere_pos = new Vector2(this.transform.position.x, this.transform.position.y);

        Debug.DrawLine(this.transform.position, clicked_pos);

        Vector2 force = clicked_pos - sphere_pos;
        force.Normalize();
        force *= 200;

        sliderjoint.angle = Vector2.Angle(Vector2.right, clicked_pos - (Vector2)character.transform.position);
        if (clicked_pos.y < character.transform.position.y)
            sliderjoint.angle *= -1;

        this.rigidbody2D.velocity = force;
        sphereState = ESphereState.CLICKED;

        // 어떤 object를 이용중이든지
        // normal상태로 강제전환
        character.interacting_type = EInteractWith.NORMAL;
        character.interacting_object = null;
        character.rigidbody2D.gravityScale = 2f;
        character.characterCanMove = true;

        shoot_pos = this.transform.position;
        last_distance = (shoot_pos - (Vector2)this.transform.position).magnitude;
		
		this.transform.SetParent(character.gameObject.transform);
    }
}
