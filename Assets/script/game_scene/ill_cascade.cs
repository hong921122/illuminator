﻿using UnityEngine;
using System;
using System.Collections;

public class ill_cascade : MonoBehaviour, IObjectTrigger
{
    public enum ECascadeState : int
    {
        ON,
        OFF
    };

    private ill_character character;

    public ECascadeState cascade_state;
    private ParticleEmitter water_particle;

    void Start()
    {
        character = GameManager.character;
        water_particle = transform.GetChild(0).GetComponent<ParticleEmitter>();
    }

    // Update is called once per frame
    void Update()
    {
        if (cascade_state == ECascadeState.ON)
        {
            this.particleEmitter.emit = true;
            water_particle.emit = true;

            if(character.interacting_type != EInteractWith.SPOTLIGHT
                && getColliderRect().Contains(character.collider2D.bounds.center))
            {
                character.rigidbody2D.AddForce(new Vector2(0.0f, -35.0f));
            }
        }
        else if (cascade_state == ECascadeState.OFF)
        {
            this.particleEmitter.emit = false;
            water_particle.emit = false;
        }
    }

    public void TriggerState()
    {
        cascade_state = (cascade_state == ECascadeState.OFF) ? ECascadeState.ON : ECascadeState.OFF;
    }

    private Rect getColliderRect()
    {
        BoxCollider2D boxCollider2D = this.gameObject.GetComponent<BoxCollider2D>();
        float worldRight = boxCollider2D.transform.TransformPoint(boxCollider2D.center + new Vector2(boxCollider2D.size.x * 0.5f, 0)).x;
        float worldLeft = boxCollider2D.transform.TransformPoint(boxCollider2D.center - new Vector2(boxCollider2D.size.x * 0.5f, 0)).x;

        float worldTop = boxCollider2D.transform.TransformPoint(boxCollider2D.center + new Vector2(0, boxCollider2D.size.y * 0.5f)).y;
        float worldBottom = boxCollider2D.transform.TransformPoint(boxCollider2D.center - new Vector2(0, boxCollider2D.size.y * 0.5f)).y;

        return new Rect(
            worldLeft,
            worldBottom,
            worldRight - worldLeft,
            worldTop - worldBottom
            );
    }
}
