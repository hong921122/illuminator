﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ill_character : MonoBehaviour
{
    private int red_energy = 0;
    private int green_energy = 0;
    private int blue_energy = 0;

    [HideInInspector]
    public bool characterCanMove = true;

    [HideInInspector]
    public EInteractWith interacting_type;

    [HideInInspector]
    public ECharacterScaleState scale_state = ECharacterScaleState.NORMAL;

    [HideInInspector]
    public GameObject interacting_object;

    private ill_lightsphere lightSphere;
    public LayerMask mask;
	public LayerMask mask2;

    // Animation
    [HideInInspector]
    public Animator anim;

    private SkeletonAnimation sa;

    private AudioSource footStepSound;

    // Use this for initialization
    void Start()
    {
        lightSphere = GameManager.sphere;

        // animation init
        footStepSound = gameObject.GetComponent<AudioSource>();

        sa = gameObject.GetComponent<SkeletonAnimation>();
        sa.loop = true;
        sa.AnimationName = "Idle";
    }

    // Update is called once per frame
    void Update()
    {
        //audio.volume = UserOptionManager.instance.VOLUME_EFFECT;

        animationRender();

        //SOUND CHECKER
        soundChecker();

        interact_obejct();
    }

    private void interact_obejct()
    {
        if (interacting_type != EInteractWith.HANGLAMP)
            Destroy(gameObject.GetComponent<DistanceJoint2D>());

        if (interacting_type != EInteractWith.STREETLAMP)
            Destroy(gameObject.GetComponent<SliderJoint2D>());

        if (interacting_type == EInteractWith.NORMAL)
        {
            characterCanMove = true;
            return;
        }

        IObjectInteraction obj = interacting_object.GetComponent(typeof(IObjectInteraction)) as IObjectInteraction;
        obj.InteractionObject(lightSphere, GetComponent<ill_character>());
    }

    public void animationRender()
    {
        Vector3 localscale = this.transform.localScale;

        if (scale_state == ECharacterScaleState.BIG)
        {
            sa.timeScale = 0.6f;
            footStepSound.pitch = 0.72f;
        }
        else if (scale_state == ECharacterScaleState.NORMAL)
        {
            sa.timeScale = 1f;
            footStepSound.pitch = 1.2f;
        }
        else if (scale_state == ECharacterScaleState.SMALL)
        {
            sa.timeScale = 1.3f;
            footStepSound.pitch = 1.56f;
        }

        if (interacting_type == EInteractWith.HANGLAMP)
        {
            sa.loop = false;
            
            if (this.rigidbody2D.velocity.y > 0.01f)
            {
                // play hook motion up
                sa.AnimationName = "Hook Front";
            }
            else if (this.rigidbody2D.velocity.y < -0.01f)
            {
                // play hook motion down
                sa.AnimationName = "Hook_Behind";
            }



            if (this.rigidbody2D.velocity.x > 0.01f)
                localscale.x = -Mathf.Abs(localscale.x);
            else if (this.rigidbody2D.velocity.x < -0.01f)
                localscale.x = Mathf.Abs(localscale.x);

            this.transform.localScale = localscale;

            return;
        }
        else if(interacting_type == EInteractWith.SPOTLIGHT)
        {
            sa.loop = true;
            sa.AnimationName = "SpotLight_Flying";

            if (this.rigidbody2D.velocity.x > 0.01f)
                localscale.x = -Mathf.Abs(localscale.x);
            else if (this.rigidbody2D.velocity.x < -0.01f)
                localscale.x = Mathf.Abs(localscale.x);

            this.transform.localScale = localscale;

            return;
        }

        if (Mathf.Abs(rigidbody2D.velocity.x) > 0.1f
            && isGrounded()
            && interacting_type == EInteractWith.NORMAL)
        {
            sa.loop = true;
            sa.AnimationName = "run2";

            return;
        }

        if (isGrounded() && rigidbody2D.velocity.magnitude < 0.1f)
        {
            sa.loop = true;
            sa.AnimationName = "Idle";

            return;
        }

        if (rigidbody2D.velocity.magnitude > 0.1f && !isGrounded() && interacting_type != EInteractWith.HANGLAMP)
        {
            if (rigidbody2D.velocity.y > 0.1f)
            {
                sa.loop = false;
                sa.AnimationName = "Jump up";
            }
            else if (rigidbody2D.velocity.y < -0.1f)
            {
                sa.loop = true;
                sa.AnimationName = "Jump down";
            }

            return;
        }
    }

    public Rect getColliderRect()
    {
        BoxCollider2D boxCollider2D = this.gameObject.GetComponent<BoxCollider2D>();
        float worldRight = boxCollider2D.transform.TransformPoint(boxCollider2D.center + new Vector2(boxCollider2D.size.x * 0.5f, 0)).x;
        float worldLeft = boxCollider2D.transform.TransformPoint(boxCollider2D.center - new Vector2(boxCollider2D.size.x * 0.5f, 0)).x;

        float worldTop = boxCollider2D.transform.TransformPoint(boxCollider2D.center + new Vector2(0, boxCollider2D.size.y * 0.5f)).y;
        float worldBottom = boxCollider2D.transform.TransformPoint(boxCollider2D.center - new Vector2(0, boxCollider2D.size.y * 0.5f)).y;

        return new Rect(
            worldLeft,
            worldBottom,
            worldRight - worldLeft,
            worldTop - worldBottom
            );
    }

    public bool isGrounded()
    {
        Rect collider_rect = getColliderRect();
        bool grounded = false;


        this.collider2D.enabled = false;

        for (int i = 0; i < 3; i++)
        {
            Vector2 step = new Vector2(collider_rect.width / 4f, 0) * i;
            Vector2 raycast_start = (Vector2)collider_rect.center - new Vector2(collider_rect.width / 4f, 0) + step;

            RaycastHit2D hit = Physics2D.Raycast(raycast_start, -Vector2.up, Mathf.Infinity, mask | mask2);

            if (hit && Vector2.Distance(raycast_start, hit.point) < collider_rect.height / 2f + 0.3f)
            {
                Debug.DrawLine(collider_rect.center, hit.point);

                grounded = true;
            }
        }

        if (!grounded && this.transform.parent != null)
            this.transform.parent = null;


        return grounded;
    }


    public void soundChecker()
    {
        if (sa.AnimationName == "run2")
        {
            if (!footStepSound.isPlaying)
            {
                footStepSound.volume = UserOptionManager.instance.VOLUME_EFFECT;
                footStepSound.Play();
            }
        }
        else
        {
            if (footStepSound.isPlaying)
            {
                footStepSound.Stop();
            }
        }
    }

    public int getEnergyCount(ELightColor color)
    {
        if(color == ELightColor.BLUE)
            return blue_energy;
        else if(color == ELightColor.RED)
            return red_energy;
        else if(color == ELightColor.GREEN)
            return green_energy;
		else 
		{
			if(blue_energy > 0 && red_energy > 0 && green_energy > 0)
				return 1;
			else return 0;
		}
    }

    public void setEnergyCount(ELightColor color, int count)
    {
        if (color == ELightColor.BLUE)
            blue_energy = count;
        else if (color == ELightColor.RED)
            red_energy = count;
        else if (color == ELightColor.GREEN)
            green_energy = count;
    }

    public void incEnergyCount(ELightColor color)
    {
        if (color == ELightColor.BLUE)
            blue_energy++;
        else if (color == ELightColor.RED)
            red_energy++;
        else if (color == ELightColor.GREEN)
            green_energy++;
		else if(color == ELightColor.WHITE) {
			red_energy++;
			green_energy++;
			blue_energy++;
		}
    }

    public void decEnergyCount(ELightColor color)
    {
        if (color == ELightColor.BLUE)
            blue_energy--;
        else if (color == ELightColor.RED)
            red_energy--;
        else if (color == ELightColor.GREEN)
            green_energy--;
		else if(color == ELightColor.WHITE)
		{
			red_energy--;
			green_energy--;
			blue_energy--;
		}
    }
}
