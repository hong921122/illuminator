﻿using UnityEngine;
using System.Collections;

public class ill_oneway_trigger : MonoBehaviour
{

    BoxCollider2D boxcol;
    bool can_pass;


    // Use this for initialization
    void Start()
    {
        boxcol = this.transform.parent.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        boxcol.enabled = !can_pass;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "character")
        {
            can_pass = true;
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "character")
        {
            can_pass = true;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "character")
        {
            can_pass = false;
        }
    }
}
