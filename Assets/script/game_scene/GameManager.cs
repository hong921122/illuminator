﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    [HideInInspector]
    public static ill_character character;
    [HideInInspector]
    public static ill_lightsphere sphere;

    public static bool clicked = false;

    private UserOptionManager om;
	private Texture2D[] energy_sprites;
	
    // inspector에서 chapter 번호 지정
    public int chapter;

    public static float SPHERE_LIGHT_LIMIT = 10.0f;
    public static float REFLECT_LIGHT_LIMIT = 10.0f;
    public static bool pause_game = false;

    
	void Awake()
    {

        character = GameObject.Find("character_newspine").GetComponent<ill_character>();
        sphere = GameObject.Find("sphere").GetComponent<ill_lightsphere>();

        Camera.main.transform.position = new Vector3(character.transform.position.x,
                                                    character.transform.position.y,
                                                    Camera.main.transform.position.z);
    }

    // Use this for initialization
    void Start()
    {

		energy_sprites = Resources.LoadAll<Texture2D>("background/energy");
        sphere.transform.position = new Vector2(character.transform.position.x + 0.8f, character.transform.position.y);
        Physics2D.IgnoreLayerCollision(9, 10);
		Physics2D.IgnoreLayerCollision(18, 12);

        om = UserOptionManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        KeyInput();
        MouseInput();
    }


    void KeyInput()
    {
        if (character.characterCanMove)
        {
            Vector3 character_scale = character.transform.localScale;

            if (Input.GetKey(om.MOVE_LEFT))
            {
                if (character.interacting_type != EInteractWith.HANGLAMP)
                {
                    Vector2 target_velocity = new Vector2(-8f, character.rigidbody2D.velocity.y);

                    float delta = target_velocity.x - character.rigidbody2D.velocity.x;

                    if (Mathf.Abs(delta) > 0.3f)
                    {
                        if (delta < 0)
                        {
                            character.rigidbody2D.velocity += new Vector2(-0.5f, 0f);
                        }
                    }
                    else
                        character.rigidbody2D.velocity = target_velocity;


                    character_scale.x = Mathf.Abs(character_scale.x);
                    character.transform.localScale = character_scale;
                }
                else
                    character.rigidbody2D.velocity += new Vector2(-0.1f, 0);

            }
            if (Input.GetKey(om.MOVE_RIGHT))
            {
                if (character.interacting_type != EInteractWith.HANGLAMP)
                {
                    Vector2 target_velocity = new Vector2(8f, character.rigidbody2D.velocity.y);
                    float delta = target_velocity.x - character.rigidbody2D.velocity.x;

                    if (Mathf.Abs(delta) > 0.3f)
                    {
                        if (delta > 0)
                        {
                            character.rigidbody2D.velocity += new Vector2(0.5f, 0f);
                        }
                    }
                    else
                        character.rigidbody2D.velocity = target_velocity;

                    character_scale.x = -Mathf.Abs(character_scale.x);
                    character.transform.localScale = character_scale;
                }
                else
                    character.rigidbody2D.velocity += new Vector2(0.1f, 0);
            }
            if (Input.GetKeyDown(om.JUMP) || Input.GetKeyDown(om.MOVE_UP))
            {
                // jump when do not jumping
                if (character.isGrounded())
                {
                    SoundEffectManager.instance.play(SoundEffectManager.instance.clip_jump);
                    character.rigidbody2D.velocity = new Vector2(character.rigidbody2D.velocity.x, 15f);
                }
            }
            // stop key if not midair
            if (Input.GetKeyDown(om.MOVE_DOWN))
            {
                if (character.isGrounded())
                {
                    character.rigidbody2D.velocity = new Vector2(0, 0);
                }
            }
            if (Input.GetKeyDown(om.STOP_OBJECT))
            {
                if (sphere.sphereState == ESphereState.HOOK)
                {
                    sphere.sphereState = ESphereState.NORMAL;
                    character.interacting_type = EInteractWith.NORMAL;
                    character.interacting_object = null;
                }
            }
        }
        if (Input.GetKey(om.SPHERE_COLOR_TYPE_1))
        {
            sphere.sphereColor = ELightColor.WHITE;
			sphere.childParticleSystem.renderer.material = sphere.childMaterial[0];
        }
        if (Input.GetKey(om.SPHERE_COLOR_TYPE_2))
        {
            sphere.sphereColor = ELightColor.RED;
			sphere.childParticleSystem.renderer.material = sphere.childMaterial[1];
        }
        if (Input.GetKey(om.SPHERE_COLOR_TYPE_3))
        {
            sphere.sphereColor = ELightColor.GREEN;
			sphere.childParticleSystem.renderer.material = sphere.childMaterial[2];
        }
        if (Input.GetKey(om.SPHERE_COLOR_TYPE_4))
        {
            sphere.sphereColor = ELightColor.BLUE;
			sphere.childParticleSystem.renderer.material = sphere.childMaterial[3];
        }
        if(Input.GetKeyDown(om.RESET))
        {
            // 현재 chapter일 때만 R키 작동
            if(chapter == SaveLoadGame.game_data.chapter_id)
                reset();
        }
        if (Input.GetKeyDown(om.USE_OBJECT))
        {
            /*
            List<GameObject> at = GameObject.FindGameObjectsWithTag("trigger");
            at.Sort(delegate(GameObject x, GameObject y) {
                return Mathf.Abs(x.transform.position - character.transform.position).
                    CompareTo(Mathf.Abs(x.transform.position - character.transform.position));
            });
            ill_trigger trigger = at[0] as ill_trigger;
            */
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Camera option_camera_transform = GameObject.Find("game_optioncamera").GetComponent<Camera>();
            if (pause_game)
            {
                //resume
                option_camera_transform.depth = -2;
                Time.timeScale = 1;
            }
            else
            {
                //pause
                option_camera_transform.depth = 2;
                Time.timeScale = 0;
            }
            pause_game = !pause_game;
        }

        Vector3 admin_pos = character.transform.position;

        // ADMIN MODE
        if(Input.GetKeyDown(KeyCode.F1))
        {
            admin_pos.x = 167.3358f;
            admin_pos.y = 41.57169f;
            character.transform.position = admin_pos;
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            admin_pos.x = 345.5f;
            admin_pos.y = 91.7f;
            character.transform.position = admin_pos;
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            admin_pos.x = 609.2f;
            admin_pos.y = 73.1f;
            character.transform.position = admin_pos;
        }
        if (Input.GetKeyDown(KeyCode.F4))
        {
            admin_pos.x = 757.8f;
            admin_pos.y = -20.7f;
            character.transform.position = admin_pos;
        }
        if (Input.GetKeyDown(KeyCode.F5))
        {
            admin_pos.x = 854.4975f;
            admin_pos.y = 89.99603f;
            character.transform.position = admin_pos;
        }
        if (Input.GetKeyDown(KeyCode.F6))
        {
            admin_pos.x = 990.6f;
            admin_pos.y = 146.2f;
            character.transform.position = admin_pos;
        }
        if (Input.GetKeyDown(KeyCode.F7))
        {
            admin_pos.x = 1224.9f;
            admin_pos.y = 147.7f;
            character.transform.position = admin_pos;
        }
        if(Input.GetKeyDown(KeyCode.I))
        {
            character.incEnergyCount(ELightColor.RED);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            character.incEnergyCount(ELightColor.GREEN);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            character.incEnergyCount(ELightColor.BLUE);
        }
        if(Input.GetKeyDown(KeyCode.L))
        {
            character.setEnergyCount(ELightColor.RED, 0);
            character.setEnergyCount(ELightColor.GREEN, 0);
            character.setEnergyCount(ELightColor.BLUE, 0);
        }

    }

    void MouseInput()
    {
        if (!clicked)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                if (hit.collider != null)
                {
                    if (hit.collider.name == "Mirror")
                        hit.collider.GetComponentInChildren<ill_mirror>().RotateLeft();
                    else if (hit.collider.name != "lighter")
                        sphere.MoveSphere(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
                }
                else
                    sphere.MoveSphere(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            }
            if (Input.GetMouseButtonDown(1))
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                if (hit.collider != null)
                {
                    if (hit.collider.name == "Mirror")
                        hit.collider.GetComponentInChildren<ill_mirror>().RotateRight();
                }
            }
        }
    }

    private void reset()
    {
        SaveLoadGame.LoadGame();

        Vector3 newpos = new Vector3(SaveLoadGame.game_data.savepoint_x,
                                    SaveLoadGame.game_data.savepoint_y,
                                    character.transform.position.z);

        sphere.sphereState = ESphereState.NORMAL;

        character.transform.position = newpos;
        character.characterCanMove = true;
        character.interacting_object = null;
        character.interacting_type = EInteractWith.NORMAL;
        character.setEnergyCount(ELightColor.RED, SaveLoadGame.game_data.red_energy);
        character.setEnergyCount(ELightColor.GREEN, SaveLoadGame.game_data.green_energy);
        character.setEnergyCount(ELightColor.BLUE, SaveLoadGame.game_data.blue_energy);

        Vector3 new_scale = character.transform.localScale;
        switch (SaveLoadGame.game_data.character_scalestate)
        {
            case 1:
                if (character.scale_state == ECharacterScaleState.NORMAL)
                {
                    new_scale.x /= 1.5f;
                    new_scale.y /= 1.5f;
                }
                else if (character.scale_state == ECharacterScaleState.BIG)
                {
                    new_scale.x /= 1.5f * 1.5f;
                    new_scale.y /= 1.5f * 1.5f;
                }
                character.scale_state = ECharacterScaleState.SMALL;
                break;

            case 2:
                if (character.scale_state == ECharacterScaleState.SMALL)
                {
                    new_scale.x *= 1.5f;
                    new_scale.y *= 1.5f;
                }
                else if (character.scale_state == ECharacterScaleState.BIG)
                {
                    new_scale.x /= 1.5f;
                    new_scale.y /= 1.5f;
                }
                character.scale_state = ECharacterScaleState.NORMAL;
                break;

            case 3:
                if (character.scale_state == ECharacterScaleState.NORMAL)
                {
                    new_scale.x *= 1.5f;
                    new_scale.y *= 1.5f;
                }
                else if (character.scale_state == ECharacterScaleState.SMALL)
                {
                    new_scale.x *= 1.5f * 1.5f;
                    new_scale.y *= 1.5f * 1.5f;
                }
                character.scale_state = ECharacterScaleState.BIG;
                break;
        }
        character.transform.localScale = new_scale;

        ill_color_energy[] arr_color_energy = Resources.FindObjectsOfTypeAll(typeof(ill_color_energy)) as ill_color_energy[];
        for (int i = 0; i < SaveLoadGame.game_data.obj_color_energy.Length; i++)
        {
            if (SaveLoadGame.game_data.obj_color_energy[i])
                arr_color_energy[i].enable();
        }

        ill_cascade[] arr_cascade = Resources.FindObjectsOfTypeAll(typeof(ill_cascade)) as ill_cascade[];
        for (int i = 0; i < SaveLoadGame.game_data.obj_cascade.Length; i++)
        {
            arr_cascade[i].cascade_state = SaveLoadGame.game_data.obj_cascade[i] ? ill_cascade.ECascadeState.ON : ill_cascade.ECascadeState.OFF;
        }

        ill_moving_ground[] arr_moving_ground = Resources.FindObjectsOfTypeAll(typeof(ill_moving_ground)) as ill_moving_ground[];
        for (int i = 0; i < SaveLoadGame.game_data.obj_moving_ground.Length; i++)
        {
            arr_moving_ground[i].ground_state = SaveLoadGame.game_data.obj_moving_ground[i] ? ill_moving_ground.EMovingGroundState.ON : ill_moving_ground.EMovingGroundState.OFF;
        }

        ill_light_wall[] arr_lightwall = Resources.FindObjectsOfTypeAll(typeof(ill_light_wall)) as ill_light_wall[];
        for(int i=0; i<SaveLoadGame.game_data.obj_lightwall.Length; i++)
        {
            arr_lightwall[i].RedFlag = SaveLoadGame.game_data.obj_lightwall[i][0];
            arr_lightwall[i].GreenFlag = SaveLoadGame.game_data.obj_lightwall[i][1];
            arr_lightwall[i].BlueFlag = SaveLoadGame.game_data.obj_lightwall[i][2];
        }

        ill_spotlight[] arr_spotlight = Resources.FindObjectsOfTypeAll(typeof(ill_spotlight)) as ill_spotlight[];
        for(int i=0; i<SaveLoadGame.game_data.obj_spotlight_angle.Length; i++)
        {
            Vector3 angle = arr_spotlight[i].transform.eulerAngles;

            arr_spotlight[i].transform.eulerAngles = new Vector3(angle.x, angle.y, SaveLoadGame.game_data.obj_spotlight_angle[i]);
        }

        ill_mirror[] arr_mirror = Resources.FindObjectsOfTypeAll(typeof(ill_mirror)) as ill_mirror[];
        for (int i = 0; i < SaveLoadGame.game_data.obj_mirror_angle.Length; i++)
        {
            Vector3 angle = arr_mirror[i].transform.eulerAngles;

            arr_mirror[i].transform.eulerAngles = new Vector3(angle.x, angle.y, SaveLoadGame.game_data.obj_mirror_angle[i]);
        }

        ill_door[] arr_door = Resources.FindObjectsOfTypeAll(typeof(ill_door)) as ill_door[];
        for(int i=0; i<SaveLoadGame.game_data.obj_door.Length; i++)
        {
            arr_door[i].Blocking = SaveLoadGame.game_data.obj_door[i];
        }
    }
}
