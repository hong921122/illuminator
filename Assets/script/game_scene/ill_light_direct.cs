﻿using UnityEngine;
using System.Collections;

public class ill_light_direct : MonoBehaviour
{

    public ELightColor lightColor;
    public SpriteRenderer colorExchanger;
    public Sprite[] colorSprites;

    // Use this for initialization
    void Start()
    {
        colorSprites = Resources.LoadAll<Sprite>("game_object/light");
        colorExchanger = gameObject.GetComponent<SpriteRenderer>();
        if (lightColor == ELightColor.WHITE) { colorExchanger.sprite = colorSprites[3]; }
        else if (lightColor == ELightColor.RED) { colorExchanger.sprite = colorSprites[2]; }
        else if (lightColor == ELightColor.GREEN) { colorExchanger.sprite = colorSprites[0]; }
        else if (lightColor == ELightColor.BLUE) { colorExchanger.sprite = colorSprites[1]; }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
