﻿using UnityEngine;
using System;
using System.Collections;

public class ill_street_lamp : MonoBehaviour, IObjectInteraction
{

    public void InteractionObject(ill_lightsphere sphere, ill_character character)
    {

        character.characterCanMove = false;

        Vector2 shootForce = this.transform.position - character.transform.position;
        //character.rigidbody2D.AddForce(shootForce * 3.0f);
        character.rigidbody2D.AddForce(shootForce.normalized * 30.0f);

        if (shootForce.magnitude < 1.0f)
        {
            character.interacting_type = EInteractWith.NORMAL;
            character.interacting_object = null;
            sphere.sphereState = ESphereState.NORMAL;

            return;
        }

        if (character.gameObject.GetComponent<SliderJoint2D>() == null)
        {
            SliderJoint2D slider_joint = character.gameObject.AddComponent<SliderJoint2D>();
            slider_joint.connectedBody = this.gameObject.rigidbody2D;
            slider_joint.connectedAnchor = Vector2.zero;
            slider_joint.anchor = Vector2.zero;
            slider_joint.angle = Vector2.Angle(Vector2.right, (Vector2)this.transform.position - new Vector2(character.transform.position.x, character.transform.position.y + 2.7f));
        }
    }
}
