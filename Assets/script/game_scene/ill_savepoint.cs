﻿using UnityEngine;
using System.Collections;

public class ill_savepoint : MonoBehaviour
{
    private bool saved = false;

    private Animator handle_animator;
    private ParticleEmitter water_particle;
    private ParticleEmitter fire_particle;

    private ill_character character;

    // Use this for initialization
    void Start()
    {
        character = GameManager.character;

        handle_animator = gameObject.transform.FindChild("Handle").GetComponent<Animator>();
        water_particle = gameObject.transform.FindChild("HoseBody").FindChild("Water Fountain").GetComponent<ParticleEmitter>();
        fire_particle = gameObject.transform.FindChild("HoseBody").FindChild("OuterCore").GetComponent<ParticleEmitter>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.CompareTag("character") || saved)
            return;

        handle_animator.SetBool("save", true);
        water_particle.emit = true;
        fire_particle.emit = true;

        SaveLoadGame.game_data.chapter_id = GameObject.Find("GameManager").GetComponent<GameManager>().chapter;
        SaveLoadGame.game_data.savepoint_x = transform.localPosition.x;
        SaveLoadGame.game_data.savepoint_y = transform.localPosition.y;
        SaveLoadGame.game_data.savepoint_z = transform.localPosition.z;
        SaveLoadGame.game_data.red_energy = character.getEnergyCount(ELightColor.RED);
        SaveLoadGame.game_data.green_energy = character.getEnergyCount(ELightColor.GREEN);
        SaveLoadGame.game_data.blue_energy = character.getEnergyCount(ELightColor.BLUE);

        if (character.scale_state == ECharacterScaleState.SMALL)
            SaveLoadGame.game_data.character_scalestate = 1;
        else if (character.scale_state == ECharacterScaleState.NORMAL)
            SaveLoadGame.game_data.character_scalestate = 2;
        else if (character.scale_state == ECharacterScaleState.BIG)
            SaveLoadGame.game_data.character_scalestate = 3;


        ill_color_energy[] arr_energy = Resources.FindObjectsOfTypeAll(typeof(ill_color_energy)) as ill_color_energy[];
        SaveLoadGame.game_data.obj_color_energy = new bool[arr_energy.Length];
        for (int i = 0; i < arr_energy.Length; i++)
        {
            SaveLoadGame.game_data.obj_color_energy[i] = arr_energy[i].valid;
        }

        ill_cascade[] arr_cascade = Resources.FindObjectsOfTypeAll(typeof(ill_cascade)) as ill_cascade[];
        SaveLoadGame.game_data.obj_cascade = new bool[arr_cascade.Length];
        for (int i = 0; i < arr_cascade.Length; i++)
        {
            SaveLoadGame.game_data.obj_cascade[i] = arr_cascade[i].cascade_state == ill_cascade.ECascadeState.ON ? true : false;
        }

        ill_moving_ground[] arr_moving_ground = Resources.FindObjectsOfTypeAll(typeof(ill_moving_ground)) as ill_moving_ground[];
        SaveLoadGame.game_data.obj_moving_ground = new bool[arr_moving_ground.Length];
        for (int i = 0; i < arr_moving_ground.Length; i++)
        {
            SaveLoadGame.game_data.obj_moving_ground[i] = arr_moving_ground[i].ground_state == ill_moving_ground.EMovingGroundState.ON ? true : false;
        }

        ill_light_wall[] arr_lightwall = Resources.FindObjectsOfTypeAll(typeof(ill_light_wall)) as ill_light_wall[];
        SaveLoadGame.game_data.obj_lightwall = new bool[arr_lightwall.Length][];
        for (int i = 0; i < arr_lightwall.Length; i++)
        {
            SaveLoadGame.game_data.obj_lightwall[i] = new bool[3];
            SaveLoadGame.game_data.obj_lightwall[i][0] = arr_lightwall[i].RedFlag;
            SaveLoadGame.game_data.obj_lightwall[i][1] = arr_lightwall[i].GreenFlag;
            SaveLoadGame.game_data.obj_lightwall[i][2] = arr_lightwall[i].BlueFlag;
        }

        ill_spotlight[] arr_spotlight = Resources.FindObjectsOfTypeAll(typeof(ill_spotlight)) as ill_spotlight[];
        SaveLoadGame.game_data.obj_spotlight_angle = new float[arr_spotlight.Length];
        for (int i = 0; i < arr_spotlight.Length; i++)
        {
            SaveLoadGame.game_data.obj_spotlight_angle[i] = arr_spotlight[i].transform.eulerAngles.z;
        }

        ill_mirror[] arr_mirror = Resources.FindObjectsOfTypeAll(typeof(ill_mirror)) as ill_mirror[];
        SaveLoadGame.game_data.obj_mirror_angle = new float[arr_mirror.Length];
        for (int i = 0; i < arr_mirror.Length; i++)
        {
            SaveLoadGame.game_data.obj_mirror_angle[i] = arr_mirror[i].transform.eulerAngles.z;
        }

        ill_door[] arr_door = Resources.FindObjectsOfTypeAll(typeof(ill_door)) as ill_door[];
        SaveLoadGame.game_data.obj_door = new bool[arr_door.Length];
        for (int i = 0; i < arr_door.Length; i++)
        {
            SaveLoadGame.game_data.obj_door[i] = arr_door[i].Blocking;
        }


        SaveLoadGame.SaveGame();

        saved = true;
    }
}
