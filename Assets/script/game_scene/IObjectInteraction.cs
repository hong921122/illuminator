﻿using UnityEngine;
using System.Collections;

public interface IObjectInteraction
{
    void InteractionObject(ill_lightsphere sphere, ill_character character);
}
