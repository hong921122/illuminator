using UnityEngine;
using System.Collections;

public class ill_mirror : MonoBehaviour
{
    [HideInInspector]
    public Vector2 mirror_normal;

    // Use this for initialization
    void Start()
    {
        mirror_normal = new Vector2(Mathf.Sin((transform.eulerAngles.z) * Mathf.Deg2Rad),
                                     Mathf.Cos((transform.eulerAngles.z) * Mathf.Deg2Rad));
    }

    // Update is called once per frame
    void Update()
    {
        mirror_normal = new Vector2(Mathf.Sin((transform.eulerAngles.z) * Mathf.Deg2Rad) * -1,
                                     Mathf.Cos((transform.eulerAngles.z) * Mathf.Deg2Rad));
    }

    public Vector3 reflect(Vector3 light_into)
    {
        return Vector3.Reflect(light_into, mirror_normal);
    }
    //50~306
    //mouse right
    public void RotateRight()
    {
        //range 설정해야함
        //if(transform.eulerAngles.z > 30)
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + 15);
    }

    //mouse left
    public void RotateLeft()
    {
        //if(transform.eulerAngles.z > 50)
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z - 15);
    }
}
