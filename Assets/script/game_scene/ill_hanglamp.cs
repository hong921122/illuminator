﻿using UnityEngine;
using System.Collections;

public class ill_hanglamp : MonoBehaviour, IObjectInteraction
{

    public void InteractionObject(ill_lightsphere sphere, ill_character character)
    {
        if (character.gameObject.GetComponent<DistanceJoint2D>() == null)
        {
            DistanceJoint2D dj = character.gameObject.AddComponent<DistanceJoint2D>();
            dj.connectedBody = this.gameObject.rigidbody2D;
            dj.connectedAnchor = Vector2.zero;
            dj.anchor = Vector2.zero;
            dj.distance = Vector2.Distance(character.transform.position, this.gameObject.transform.position);
        }
    }
}
