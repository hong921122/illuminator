﻿using UnityEngine;
using System.Collections;

public class GuiCamera : MonoBehaviour {
	
	public GUIStyle label_style;

	public ill_character character;
	
	public GameObject[] guiParticle;

	// Use this for initialization
	void Start () {

		character = GameManager.character;

		label_style.fontStyle = FontStyle.Bold;
		label_style.normal.textColor = Color.white;
		label_style.fontSize = (int)(Screen.height * 0.05f);

	}
	
	// Update is called once per frame
	void Update () {
		
		for (int i=0; i<3; i++) {
			guiParticle[i].transform.localPosition = new Vector3(-(getScreenSizeInWorldCoordinate().x*0.45f)+(getScreenSizeInWorldCoordinate().x*i*0.1f), getScreenSizeInWorldCoordinate().y/2-2.0f, 1.0f);
			guiParticle[i].particleEmitter.maxSize = (getScreenSizeInWorldCoordinate().y*0.17f) + 1.0f;
			guiParticle[i].particleEmitter.minSize = (getScreenSizeInWorldCoordinate().y*0.17f);
			//Debug.Log (i);
			//Debug.Log (guiParticle[i].transform.position);
		}
	}

	Vector2 getScreenSizeInWorldCoordinate()
	{
		Camera cam = Camera.main;
		Vector3 p1 = cam.ViewportToWorldPoint(new Vector3(0, 0, cam.nearClipPlane));
		Vector3 p2 = cam.ViewportToWorldPoint(new Vector3(1, 0, cam.nearClipPlane));
		Vector3 p3 = cam.ViewportToWorldPoint(new Vector3(1, 1, cam.nearClipPlane));
		
		float width = (p2 - p1).magnitude;
		float height = (p3 - p2).magnitude;
		
		return new Vector2(width, height);
	}

	void OnGUI() 
	{
		//GUI.DrawTexture(new Rect(10, 10, 50, 50), energy_sprites[2]);
		GUI.Label(new Rect(Screen.width * 0.07f, Screen.height* 0.15f, 10, 10), character.getEnergyCount(ELightColor.RED).ToString(), label_style);
		//GUI.DrawTexture(new Rect(110, 10, 50, 50), energy_sprites[1]);
		GUI.Label(new Rect(Screen.width * 0.17f, Screen.height* 0.15f, 10, 10), character.getEnergyCount(ELightColor.GREEN).ToString(), label_style);
		//GUI.DrawTexture(new Rect(210, 10, 50, 50), energy_sprites[0]);
		GUI.Label(new Rect(Screen.width * 0.27f, Screen.height* 0.15f, 10, 10), character.getEnergyCount(ELightColor.BLUE).ToString(), label_style);
	}
}
