﻿using UnityEngine;
using System.Collections;

public class PauseMenuMouseOver : MonoBehaviour
{
    private SpriteRenderer sprite;

    void Start()
    {
        sprite = this.GetComponent<SpriteRenderer>();
    }

    void OnMouseEnter()
    {
        sprite.color = new Color(255, 0, 0);
    }

    void OnMouseExit()
    {
        sprite.color = new Color(255, 255, 255);
    }

    void OnMouseDown()
    {
        if (gameObject.name == "resume")
        {
            //resume
			Camera option_camera_transform = GameObject.Find("game_optioncamera").GetComponent<Camera>();
			option_camera_transform.depth = -2;
			Time.timeScale = 1;
			GameManager.pause_game = !GameManager.pause_game;
        }
        if (gameObject.name == "option")
        {
            //TODO
        }
        if (gameObject.name == "exit")
        {
			Time.timeScale = 1;
			Application.LoadLevel("initial_scene");
//            StartCoroutine("SelectMenu", "initial_scene");
        }
    }

    private IEnumerator SelectMenu(string level)
    {
        yield return new WaitForSeconds(0.6f);
//        float fade_time = GameObject.Find("background_camera").GetComponent<FadeScene>().BeginFade(1, true);
//        yield return new WaitForSeconds(fade_time);
        Application.LoadLevel(level);
    }

    private IEnumerator SelectMenuFadeWhite(string level)
    {
        yield return new WaitForSeconds(0.6f);
        GameObject.Find("Main Camera").GetComponent<FadeScene>().fade_speed = 3.0f;
        float fade_time = GameObject.Find("background_camera").GetComponent<FadeScene>().BeginFade(1, false);
        yield return new WaitForSeconds(fade_time);
        Application.LoadLevel(level);
    }
}
