﻿using UnityEngine;
using System.Collections;

public class ill_laser_sensor : MonoBehaviour {
	public ELightColor sensor_color;

    [HideInInspector]
	public bool laser_sensing;

	public GameObject sensing_object;
	private GameObject laser_path;

	// Use this for initialization
	void Start () {
		laser_sensing = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(laser_sensing)
		{
			if(laser_path.transform.parent.GetComponent<ill_laser>().laser_color == sensor_color) //check same laser color
				sensing_object.GetComponent<ill_light_wall>().sensor_laser(sensor_color);
		}
	}

	void OnTriggerEnter2D(Collider2D col) 
	{
		if(col.gameObject.tag == "laser")
		{
			laser_path = col.gameObject;
			laser_sensing = true;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		laser_sensing = false;
	}
}
