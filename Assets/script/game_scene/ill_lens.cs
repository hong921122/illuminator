﻿using UnityEngine;
using System.Collections;

public class ill_lens : MonoBehaviour
{
    private ill_character character;
    private ill_lightsphere sphere;

    private bool character_trigger_flag;
    public ELensType lenstype;

    public Sprite concave;
    public Sprite convex;

    // Use this for initialization
    void Start()
    {
        character_trigger_flag = false;
        character = GameManager.character;
        sphere = GameManager.sphere;

        if (lenstype == ELensType.CONCAVE)
            //GetComponent<SpriteRenderer>().sprite = Resources.LoadAssetAtPath<Sprite>("Assets/Resources/game_object/lens/lens_concave.png");
            GetComponent<SpriteRenderer>().sprite = concave;
        else
            //GetComponent<SpriteRenderer>().sprite = Resources.LoadAssetAtPath<Sprite>("Assets/Resources/game_object/lens/lens_convex.png");
            GetComponent<SpriteRenderer>().sprite = convex;

        BoxCollider2D a = this.gameObject.AddComponent<BoxCollider2D>();
        a.isTrigger = true;
        a.center = new Vector2(0.0f, 0.0f);
        a.size = new Vector2(GetComponent<SpriteRenderer>().sprite.bounds.size.x,
            GetComponent<SpriteRenderer>().sprite.bounds.size.y);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.collider2D.tag == "character" && !character_trigger_flag)
        {
            character_trigger_flag = true;
        }
        else
        {
            return;
        }

        if (lenstype == ELensType.CONVEX && character.scale_state != ECharacterScaleState.SMALL)
        {
            SoundEffectManager.instance.play(SoundEffectManager.instance.clip_lens_smaller);

            character.GetComponent<CharacterLensAnim>().renderAnimation(ELensType.CONVEX);
            sphere.GetComponent<CharacterLensAnim>().renderAnimation(ELensType.CONVEX);
            if (character.scale_state == ECharacterScaleState.NORMAL)
            {
                character.scale_state = ECharacterScaleState.SMALL;
            }
            else
            {
                character.scale_state = ECharacterScaleState.NORMAL;
            }
        }
        else if (lenstype == ELensType.CONCAVE && character.scale_state != ECharacterScaleState.BIG)
        {
            SoundEffectManager.instance.play(SoundEffectManager.instance.clip_lens_bigger);

            character.GetComponent<CharacterLensAnim>().renderAnimation(ELensType.CONCAVE);
            sphere.GetComponent<CharacterLensAnim>().renderAnimation(ELensType.CONCAVE);
            if (character.scale_state == ECharacterScaleState.NORMAL)
            {
                character.scale_state = ECharacterScaleState.BIG;
            }
            else
            {
                character.scale_state = ECharacterScaleState.NORMAL;
            }
        }

    }

    void OnTriggerExit2D(Collider2D col)
    {
        character_trigger_flag = false;
    }
}
