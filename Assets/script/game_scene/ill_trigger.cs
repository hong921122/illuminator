﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ill_trigger : MonoBehaviour
{
    //	public IObjectTrigger trigger_object;
    public enum ETriggerType
    {
        LEVER,
        BUTTON
    };

	public List<GameObject> trigger_gameObj;
//    public GameObject[] trigger_gameObj;
    public float speed;
    public ETriggerType trigger_type;
    public float angle_offset;
    private bool lever_state;
    private Animator lever_animator;

    private ill_character character;
	private int trigger_obj_cnt;

    //가까운 위치에 있는 트리거만 작동
    // Use this for initialization
    void Start()
    {
        lever_state = true;
        lever_animator = GetComponent<Animator>();
        character = GameManager.character;

        speed = 3.0f;
    }

    // Update is called once per frame
    void Update()
    {
		trigger_obj_cnt = trigger_gameObj.Count;
        //object render
        if (Input.GetKeyDown(UserOptionManager.instance.USE_OBJECT) && trigger_type == ETriggerType.LEVER)
        {
            Transform character_pos = character.transform;
            if (Vector3.Distance(character_pos.position, this.transform.position) < 5)
            {
                SoundEffectManager.instance.play(SoundEffectManager.instance.clip_trigger);

                lever_animator.SetBool("lever_state", lever_state);
                lever_state = !lever_state;
                angle_offset = (angle_offset == 320) ? 40 : 320;

                if(trigger_gameObj.Count > 0)
                    StartCoroutine("disable_triggered");
            }
        }
    }

	public IEnumerator disable_triggered()
	{
		foreach(GameObject gameobj in trigger_gameObj)
		{
			IObjectTrigger trigger = gameobj.GetComponent(typeof(IObjectTrigger)) as IObjectTrigger;
			trigger.TriggerState();					
		}
		GameCamera cam = Camera.main.GetComponent<GameCamera>();
		cam.showing_triggered = true;
		cam.showing_obj = trigger_gameObj;
		yield return new WaitForSeconds(2.0f);
		Camera.main.GetComponent<GameCamera>().showing_triggered = false;
	}

    public void disable_showing_triggered()
    {
		Debug.Log (trigger_obj_cnt);
		trigger_obj_cnt --;
		if(trigger_obj_cnt == 0)
        	Camera.main.GetComponent<GameCamera>().showing_triggered = false;
    }
}
