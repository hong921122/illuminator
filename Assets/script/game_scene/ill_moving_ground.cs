﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class ill_moving_ground : MonoBehaviour , IObjectTrigger{
	public enum EMovingGroundState : int
	{
		ON,
		OFF
	};

	public Vector3 start_transform;
	public Vector3 end_transform;
	public float move_time;
	public float stand_by_time;
	public bool move_ground;
	public EMovingGroundState ground_state;

	private ill_character character;

	// Use this for initialization
	void Start () {
		ground_state = EMovingGroundState.ON;
		character = GameManager.character;
		move_ground = false;
		renderAnimation();
	}
	
	// Update is called once per frame
	void Update () {
		if(ground_state == EMovingGroundState.ON)
		{
			animation["anim"].speed = 1.0f;
		}
		else if(ground_state == EMovingGroundState.OFF)
		{
			animation["anim"].speed = 0.0f;
		}
	}

	void OnCollisionStay2D(Collision2D col) {
		col.collider.transform.SetParent(this.gameObject.transform);
		col.transform.localPosition = new Vector3 (col.transform.localPosition.x, col.transform.localPosition.y, 0.0f);
	}

	void OnCollisionExit2D(Collision2D col) {
		col.collider.transform.parent = null;
		col.transform.localPosition = new Vector3 (col.transform.localPosition.x, col.transform.localPosition.y, 0.0f);
	}

	private void renderAnimation() {
		AnimationCurve curve_x = AnimationCurve.EaseInOut(0.0f, start_transform.x, 2 * (move_time + stand_by_time), start_transform.x);
		curve_x.AddKey(move_time, end_transform.x);
		curve_x.AddKey(move_time + stand_by_time, end_transform.x);
		curve_x.AddKey(2*move_time + stand_by_time, start_transform.x);

		AnimationCurve curve_y = AnimationCurve.EaseInOut(0.0f, start_transform.y, 2 * (move_time + stand_by_time), start_transform.y);
		curve_y.AddKey(move_time, end_transform.y);
		curve_y.AddKey(move_time + stand_by_time, end_transform.y);
		curve_y.AddKey(2*move_time + stand_by_time, start_transform.y);

		AnimationClip clip = new AnimationClip();
		clip.SetCurve("", typeof(Transform), "localPosition.x", curve_x);
		clip.SetCurve("", typeof(Transform), "localPosition.y", curve_y);
		animation.AddClip(clip, "anim");
		animation.wrapMode = WrapMode.Loop;
		animation.Play("anim");
	}

	public void TriggerState()
	{
		ground_state = (ground_state == EMovingGroundState.OFF) ? EMovingGroundState.ON : EMovingGroundState.OFF;
	}
}
