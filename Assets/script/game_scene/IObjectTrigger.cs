﻿using UnityEngine;
using System.Collections;

public interface IObjectTrigger
{
    void TriggerState();
}
