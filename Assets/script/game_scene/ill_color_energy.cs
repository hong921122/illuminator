﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Animation))]
public class ill_color_energy : MonoBehaviour
{
    public ELightColor energy_color;
    private ill_character character;

	private ParticleRenderer particleRenderer;
	private List<Material> particleMaterial;

    [HideInInspector]
    public bool valid = true;

    public Material white;
    public Material red;
    public Material green;
    public Material blue;

    // Use this for initialization
    void Start()
    {
        character = GameManager.character;

		particleRenderer = this.GetComponent<ParticleRenderer> ();

		particleMaterial = new List<Material>();
        //particleMaterial.Add(Resources.LoadAssetAtPath<Material>("Assets/Material/lightEnergy_white.mat"));
        //particleMaterial.Add(Resources.LoadAssetAtPath<Material>("Assets/Material/lightEnergy_red.mat"));
        //particleMaterial.Add(Resources.LoadAssetAtPath<Material>("Assets/Material/lightEnergy_green.mat"));
        //particleMaterial.Add(Resources.LoadAssetAtPath<Material>("Assets/Material/lightEnergy_blue.mat"));

        particleMaterial.Add(white);
        particleMaterial.Add(red);
        particleMaterial.Add(green);
        particleMaterial.Add(blue);


		
		if (energy_color == ELightColor.WHITE)
			particleRenderer.renderer.material = particleMaterial[0];
        if (energy_color == ELightColor.RED)
			particleRenderer.renderer.material = particleMaterial[1];
		if(energy_color == ELightColor.GREEN)
			particleRenderer.renderer.material = particleMaterial[2];
		if(energy_color == ELightColor.BLUE)
			particleRenderer.renderer.material = particleMaterial[3];
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag != "character" || !valid)
            return;

        valid = false;

        //exec vanishAnimation
        vanishAnimation();

        character.incEnergyCount(energy_color);
    }

    void vanishAnimation()
    {
        animation["vanish"].speed = 1f;
        animation.Play("vanish");
        SoundEffectManager.instance.play(SoundEffectManager.instance.clip_energy_disapear);
        Invoke("disable", 0.6f);
    }

    public void disable()
    {
        // Destroy(this.gameObject);

        this.GetComponent<ParticleEmitter>().emit = false;
        this.transform.GetChild(0).GetComponent<Light>().enabled = false;
        this.transform.GetChild(1).GetComponent<ParticleEmitter>().emit = false;
        this.transform.GetChild(2).GetComponent<ParticleEmitter>().emit = false;
    }

    public void enable()
    {
        valid = true;

        this.GetComponent<Animation>()["vanish"].speed = -10f;
        this.GetComponent<Animation>().Play("vanish");

        this.GetComponent<ParticleEmitter>().emit = true;
        this.transform.GetChild(0).GetComponent<Light>().enabled = true;
        this.transform.GetChild(1).GetComponent<ParticleEmitter>().emit = true;
        this.transform.GetChild(2).GetComponent<ParticleEmitter>().emit = true;
    }
}
