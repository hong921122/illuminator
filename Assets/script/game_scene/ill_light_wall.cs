﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ill_light_wall : MonoBehaviour
{
    public ill_laser_sensor[] laser_sensor;

    private bool red_flag = true;
    private bool green_flag = true;
    private bool blue_flag = true;
    //private List<ParticleEmitter> emits;
    private ParticleEmitter[] emits;

    public bool RedFlag
    {
        set
        {
            if(value && !red_flag)
            {
                emits[0].emit = false;
                emits[0].animation["wall_vanish"].speed = 1;
                emits[0].animation.Play("wall_vanish");
            }
            else if(!value && red_flag)
            {
                emits[0].emit = true;
                emits[0].animation["wall_vanish"].speed = -1;
                emits[0].animation.Play("wall_vanish");
            }

            red_flag = value;
        }
        get
        {
            return red_flag;
        }
    }

    public bool GreenFlag
    {
        set
        {
            if (value && !green_flag)
            {
                emits[1].emit = false;
                emits[1].animation["wall_vanish"].speed = 1;
                emits[1].animation.Play("wall_vanish");
            }
            else if (!value && green_flag)
            {
                emits[1].emit = true;
                emits[1].animation["wall_vanish"].speed = -1;
                emits[1].animation.Play("wall_vanish");
            }

            green_flag = value;
        }
        get
        {
            return green_flag;
        }
    }

    public bool BlueFlag
    {
        set
        {
            if (value && !blue_flag)
            {
                emits[2].emit = false;
                emits[2].animation["wall_vanish"].speed = 1;
                emits[2].animation.Play("wall_vanish");
            }
            else if (!value && blue_flag)
            {
                emits[2].emit = true;
                emits[2].animation["wall_vanish"].speed = -1;
                emits[2].animation.Play("wall_vanish");
            }

            blue_flag = value;
        }
        get
        {
            return blue_flag;
        }
    }

    // Use this for initialization
    void Start()
    {
        emits = GetComponentsInChildren<ParticleEmitter>();
        //emits = new List<ParticleEmitter>();
        
        //foreach (ParticleEmitter tr in GetComponentsInChildren<ParticleEmitter>())
        //{
        //    if(tr.gameObject.name == "light_energy")
        //        emits.Add(tr);
        //}

        

        

        foreach (ParticleEmitter emit in emits)
            emit.emit = false;

        foreach (ill_laser_sensor obj in laser_sensor)
        {
            if (obj.sensor_color == ELightColor.RED)
            {
                RedFlag = false;
            }
            if (obj.sensor_color == ELightColor.GREEN)
            {
                GreenFlag = false;
            }
            if (obj.sensor_color == ELightColor.BLUE)
            {
                BlueFlag = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (RedFlag && GreenFlag && BlueFlag)
        {
            this.collider2D.enabled = false;
            this.transform.FindChild("GodHaze").gameObject.SetActive(false);
        }
        else
        {
            this.collider2D.enabled = true;
            this.transform.FindChild("GodHaze").gameObject.SetActive(true);
        }
    }

    public void sensor_laser(ELightColor color)
    {
        switch(color)
        {
            case ELightColor.RED:
                RedFlag = true;
                break;

            case ELightColor.GREEN:
                GreenFlag = true;
                break;

            case ELightColor.BLUE:
                BlueFlag = true;
                break;
        }
    }
}
