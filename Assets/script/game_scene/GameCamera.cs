using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameCamera : MonoBehaviour
{

    private ill_character character;
    private Rect map_boundary;

    [HideInInspector]
    public bool showing_triggered;

    [HideInInspector]
    public List<GameObject> showing_obj;

    public GameObject background_prefab;
    private List<GameObject> backgrounds;
    public GameObject background_sky_prefab;
    private GameObject background_sky;

    private float default_camsize;


    // Use this for initialization
    void Start()
    {

        character = GameManager.character;
        default_camsize = Camera.main.orthographicSize;

        //background_prefab = Resources.LoadAssetAtPath<GameObject>("Assets/object_prefab/Background.prefab");
        //background_sky_prefab = Resources.LoadAssetAtPath<GameObject>("Assets/object_prefab/bgsky.prefab");

        initBackground();
        initBackgroundSky();

        GameObject[] grounds = GameObject.FindGameObjectsWithTag("ground");
        map_boundary = new Rect(0, 0, 0, 0);

        // map boundary 설정
        foreach (GameObject ground in grounds)
        {
            if (map_boundary.xMin > ground.transform.position.x)
                map_boundary.xMin = ground.transform.position.x;

            if (map_boundary.xMax < ground.transform.position.x)
                map_boundary.xMax = ground.transform.position.x;

            if (map_boundary.yMin > ground.transform.position.y)
                map_boundary.yMin = ground.transform.position.y;

            if (map_boundary.yMax < ground.transform.position.y)
                map_boundary.yMax = ground.transform.position.y;
        }
    }

    private void initBackground()
    {
        Vector3 camera_pos = Camera.main.transform.position;
        Vector3 bg_position = new Vector3(camera_pos.x, camera_pos.y, 1.0f);

        backgrounds = new List<GameObject>();

        for (int i = 0; i < 3; i++)
        {
            GameObject clone = Instantiate(background_prefab, bg_position, Quaternion.identity) as GameObject;

            Transform tr = clone.transform;
            Renderer sr = clone.renderer;

            tr.transform.localScale = new Vector3(1, 1, 1);

            float width = sr.bounds.size.x;
            float height = sr.bounds.size.y;

            float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
            float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

            tr.transform.localScale = new Vector3(worldScreenWidth / width * 2.0f, worldScreenHeight / height/* * 1.1f*/, tr.transform.localScale.z);

            backgrounds.Add(clone);
        }
    }

    private void initBackgroundSky()
    {
        Vector3 camera_pos = Camera.main.transform.position;
        Vector3 bg_position = new Vector3(camera_pos.x, camera_pos.y, 1.5f);
        GameObject clone = Instantiate(background_sky_prefab, bg_position, Quaternion.identity) as GameObject;

        Transform tr = clone.transform;
        Renderer sr = clone.renderer;

        tr.transform.localScale = new Vector3(1, 1, 1);

        float width = sr.bounds.size.x;
        float height = sr.bounds.size.y;

        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        tr.transform.localScale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, tr.transform.localScale.z);

        background_sky = clone;
    }

    // Update is called once per frame
    void Update()
    {

        //audioSource vol
        this.gameObject.GetComponent<AudioSource>().volume = UserOptionManager.instance.VOLUME_BGM;

        GameObject target;
        Vector3 new_campos = this.transform.position;

        float screen_width = getScreenSizeInWorldCoordinate().x;
        float screen_height = getScreenSizeInWorldCoordinate().y;

        Transform tr = background_sky.transform;
        Renderer sr = background_sky.renderer;

        tr.transform.localScale = new Vector3(1, 1, 1);
        float width = sr.bounds.size.x;
        float height = sr.bounds.size.y;
        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        tr.transform.localScale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, tr.transform.localScale.z);

        // trigger 조작 시 연결된 object를 잠시 비춤
        // 보통때는 character를 비춤
        #region
        if (showing_triggered)
        {
            if (showing_obj.Count == 1)
            {
                target = showing_obj[0];
                new_campos = new Vector3(target.transform.position.x,
                                            target.transform.position.y,
                                            this.transform.position.z);
            }
            else
            {
                Rect show_range = new Rect(showing_obj[0].transform.position.x, showing_obj[0].transform.position.y, 0, 0);
                List<Vector3> pos = new List<Vector3>();

                // insert positions for setting Rect range
                foreach(GameObject obj in showing_obj)
                {
                    if (obj.tag == "waterfall")
                    {
                        pos.Add(obj.transform.position);
                    }
                    else if(obj.tag == "ground")
                    {
                        pos.Add(obj.GetComponent<ill_moving_ground>().start_transform);
                        pos.Add(obj.GetComponent<ill_moving_ground>().end_transform);
                    }
                }

                // set rect range
                foreach (Vector3 p in pos)
                {
                    if (show_range.xMin > p.x)
                        show_range.xMin = p.x;

                    if (show_range.xMax < p.x)
                        show_range.xMax = p.x;

                    if (show_range.yMin > p.y)
                        show_range.yMin = p.y;

                    if (show_range.yMax < p.y)
                        show_range.yMax = p.y;
                }

                Debug.DrawLine(show_range.max, show_range.min);

                Camera.main.orthographicSize = show_range.height * 2 / 3;
                new_campos = new Vector3(show_range.center.x,
                                                            show_range.center.y,
                                                            Camera.main.transform.position.z);

            }
        }
        else
        {
            Camera.main.orthographicSize = default_camsize;
            target = character.gameObject;
            Vector2 target_pos = new Vector3(target.transform.position.x, target.transform.position.y + 8f);


            if (this.transform.position.x - target_pos.x > screen_width / 10)
                new_campos.x = target_pos.x + screen_width / 10;
            else if (this.transform.position.x - target_pos.x < -screen_width / 10)
                new_campos.x = target_pos.x - screen_width / 10;

            if (this.transform.position.y - target_pos.y > screen_height / 10)
                new_campos.y = target_pos.y + screen_height / 10;
            else if (this.transform.position.y - target_pos.y < -screen_height / 10)
                new_campos.y = target_pos.y - screen_height / 10;

            //new_campos = new Vector3(new_campos.x, new_campos.y + 1f, new_campos.z);
        }
        #endregion


        // 카메라 촬영 범위를 전체 map의 boundary 내로 한정
        #region
        if (new_campos.x - (screen_width) / 2 < this.map_boundary.xMin)
            new_campos.x = this.map_boundary.xMin + (screen_width) / 2;

        if (new_campos.x + (screen_width) / 2 > this.map_boundary.xMax)
            new_campos.x = this.map_boundary.xMax - (screen_width) / 2;

        if (new_campos.y - (screen_height) / 2 < this.map_boundary.yMin)
            new_campos.y = this.map_boundary.yMin + (screen_height) / 2;

        if (new_campos.y + (screen_height) / 2 > this.map_boundary.yMax)
            new_campos.y = this.map_boundary.yMax - (screen_height) / 2;
        #endregion


        // 카메라 움직임을 부드럽게
        #region
        Vector3 cam_delta = (new_campos - this.transform.position) / 20f;
        this.rigidbody2D.velocity = ((Vector2)new_campos - (Vector2)this.transform.position) * 10;
        #endregion


        // 산 배경 이미지 정렬.
        // 현재 비추는 산 배경 이미지의 좌우로 정렬
        #region

        Camera cam = Camera.main;

        Vector3 bgsize = backgrounds[0].GetComponent<SpriteRenderer>().bounds.size;

        Vector3 cam_left_top = new Vector3(cam.transform.position.x - worldScreenWidth / 2.0f,
                                            cam.transform.position.y + worldScreenHeight / 2.0f,
                                            1.0f);
        Vector3 cam_right_bottom = new Vector3(cam.transform.position.x + worldScreenWidth / 2.0f,
                                        cam.transform.position.y - worldScreenHeight / 2.0f,
                                        1.0f);

        GameObject current_viewing_bg = null;

        foreach (GameObject bg in backgrounds)
        {
            Bounds bg_bounds = bg.GetComponent<SpriteRenderer>().bounds;

            if (bg_bounds.Contains(new Vector3(cam.transform.position.x, cam.transform.position.y, 1.0f)))
            {
                Vector3 new_bgpos = bg.transform.position + cam_delta;

                //// 카메라가 상하로 움직여도 산 배경 이미지는 카메라를 벗어나지 않도록
                //if (new_bgpos.y - bgsize.y / 2 > cam_right_bottom.y)
                //    new_bgpos.y = cam_right_bottom.y + bgsize.y / 2;

                //if (new_bgpos.y + bgsize.y / 2 < cam_left_top.y)
                //    new_bgpos.y = cam_left_top.y - bgsize.y / 2;


                //current_viewing_bg = bg;
                //current_viewing_bg.transform.position = new_bgpos;


                current_viewing_bg = bg;
                current_viewing_bg.transform.position = new_bgpos;
                current_viewing_bg.transform.position = new Vector3(current_viewing_bg.transform.position.x, cam.transform.position.y, current_viewing_bg.transform.position.z);

                //Debug.DrawLine(bg_bounds.max, bg_bounds.min);

                break;
            }
        }

        if (current_viewing_bg != null)
        {
            for (int i = 0, j = 0; i < 3; i++)
            {
                GameObject bg = backgrounds[i];

                if (bg == current_viewing_bg)
                    continue;

                switch (j++)
                {
                    case 0: // left
                        bg.transform.position = new Vector3(current_viewing_bg.transform.position.x - bgsize.x + 1f,
                                                        current_viewing_bg.transform.position.y,
                                                        bg.transform.position.z);
                        break;

                    case 1: // right
                        bg.transform.position = new Vector3(current_viewing_bg.transform.position.x + bgsize.x - 1f,
                                                    current_viewing_bg.transform.position.y,
                                                    bg.transform.position.z);
                        break;
                }
            }
        }

        #endregion


        // 하늘배경 고정
        background_sky.transform.position = new Vector3(this.transform.position.x,
                                                            this.transform.position.y,
                                                            background_sky.transform.position.z);
    }

    Vector2 getScreenSizeInWorldCoordinate()
    {
        Camera cam = Camera.main;
        Vector3 p1 = cam.ViewportToWorldPoint(new Vector3(0, 0, cam.nearClipPlane));
        Vector3 p2 = cam.ViewportToWorldPoint(new Vector3(1, 0, cam.nearClipPlane));
        Vector3 p3 = cam.ViewportToWorldPoint(new Vector3(1, 1, cam.nearClipPlane));

        float width = (p2 - p1).magnitude;
        float height = (p3 - p2).magnitude;

        return new Vector2(width, height);
    }
	

}
