﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class CharacterLensAnim : MonoBehaviour
{
    public void renderAnimation(ELensType type)
    {
        //animation
        if (type == ELensType.CONCAVE)
        {
            AnimationCurve curve1 = AnimationCurve.EaseInOut(0.0f, transform.localScale.x,
                2.0f, transform.localScale.x * 1.5f);
            AnimationCurve curve2 = AnimationCurve.EaseInOut(0.0f, transform.localScale.y,
                2.0f, transform.localScale.y * 1.5f);

            AnimationClip clip = new AnimationClip();
            clip.SetCurve("", typeof(Transform), "localScale.x", curve1);
            clip.SetCurve("", typeof(Transform), "localScale.y", curve2);
            animation.AddClip(clip, "CONCAVE");
            animation.Play("CONCAVE");
        }
        else if (type == ELensType.CONVEX)
        {
            AnimationCurve curve1 = AnimationCurve.EaseInOut(0.0f, transform.localScale.x,
                2.0f, transform.localScale.x / 1.5f);
            AnimationCurve curve2 = AnimationCurve.EaseInOut(0.0f, transform.localScale.y,
                2.0f, transform.localScale.y / 1.5f);

            AnimationClip clip = new AnimationClip();
            clip.SetCurve("", typeof(Transform), "localScale.x", curve1);
            clip.SetCurve("", typeof(Transform), "localScale.y", curve2);
            animation.AddClip(clip, "CONVEX");
            animation.Play("CONVEX");
        }
    }
}
