﻿using UnityEngine;
using System.Collections;

public class ill_portal : MonoBehaviour
{
    public ill_portal connect_portal;

    [HideInInspector]
    public EDirection portalDirection;

    // Use this for initialization
    void Start()
    {
        if (this.transform.parent.transform.localScale.x > 0)
        {
            portalDirection = EDirection.LEFT;
        }
        else if (this.transform.parent.transform.localScale.x < 0)
        {
            portalDirection = EDirection.RIGHT;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (connect_portal == null)
            return;

        if (col.gameObject.tag == "character")
        {
            SoundEffectManager.instance.play(SoundEffectManager.instance.clip_portal);

            if (connect_portal.portalDirection == this.portalDirection)
            {
                col.rigidbody2D.velocity = new Vector2(-col.rigidbody2D.velocity.x, col.rigidbody2D.velocity.y);
            }

            if (connect_portal.portalDirection == EDirection.RIGHT)
            {
                col.gameObject.transform.position = new Vector2(connect_portal.transform.position.x + 3.0f, connect_portal.transform.position.y - 2.0f);
            }
            else if (connect_portal.portalDirection == EDirection.LEFT)
            {
                col.gameObject.transform.position = new Vector2(connect_portal.transform.position.x - 3.0f, connect_portal.transform.position.y - 2.0f);
            }
        }
    }

}
