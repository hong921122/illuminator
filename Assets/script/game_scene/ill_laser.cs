﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ill_laser : MonoBehaviour
{
	
	public LayerMask ground_mask;
	public LayerMask portal_mask;
	public LayerMask object_mask;
	public LayerMask movingGround_mask;
	public GameObject light_prefab;
	public Vector3 laser_normal;
    public ELightColor laser_color;

	private Vector2 light_direction;

	[HideInInspector]
	public List<List<Vector2>> point_list;
	
//	private Transform spotlight_holder;
	
	private Vector3 screenPos;
	private float angleOffset;
	
	private int curr_start_index;
	
	private ill_character character;
	
	// Use this for initialization
	void Start()
	{
		character = GameManager.character;
		
//		spotlight_holder = transform.parent.GetChild(1);
		curr_start_index = 0;
	}
	
	// Update is called once per frame
	void Update()
	{
		DrawBeam();
	}
	
	void DrawBeam()
	{
		// draw ray including reflected
		light_direction = new Vector2(Mathf.Cos(transform.eulerAngles.z * Mathf.Deg2Rad),
		                              Mathf.Sin(transform.eulerAngles.z * Mathf.Deg2Rad));
		RaycastHit2D hit;
		Vector3 current_pos = this.transform.position;
		Vector3 current_direction = light_direction;
		
//		collider2D.enabled = false;
//		spotlight_holder.collider2D.enabled = false;
		hit = Physics2D.Raycast(current_pos, current_direction, Mathf.Infinity, ground_mask | object_mask | portal_mask | movingGround_mask);
//		collider2D.enabled = true;
//		spotlight_holder.collider2D.enabled = true;
		
		// points for rendering
		point_list = new List<List<Vector2>>();
        point_list.Add(new List<Vector2>());
		point_list[0].Add((Vector2)current_pos + light_direction.normalized * 1.6f);
		
		// get reflected points
		while (hit.collider != null)
		{
			if (hit.collider.tag == "mirror")
			{
				Debug.DrawLine(current_pos, hit.point, Color.red);
				point_list[point_list.Count-1].Add(hit.point);
				
				ill_mirror mirror = hit.collider.gameObject.GetComponent<ill_mirror>();
				
				current_pos = hit.point + mirror.mirror_normal * 0.01f;
				current_direction = mirror.reflect(current_direction);
				
				// plus or minus
				hit = Physics2D.Raycast(current_pos, current_direction, Mathf.Infinity, ground_mask | object_mask | portal_mask | movingGround_mask);
			}
			else if (hit.collider.tag == "portal") 
			{
				Debug.Log ("portal");
				point_list[point_list.Count-1].Add(hit.point);

                ill_portal conn_portal = hit.collider.gameObject.GetComponent<ill_portal>().connect_portal;

                if(conn_portal == null)
                    break;


				current_pos = conn_portal.transform.position;
                point_list.Add(new List<Vector2>());
                point_list[point_list.Count-1].Add(current_pos);

                if (conn_portal.portalDirection == EDirection.LEFT)
                {
                    current_direction.x = -Mathf.Abs(current_direction.x);
                    current_pos.x -= 1.5f;
                }
                else
                {
                    current_direction.x = Mathf.Abs(current_direction.x);
                    current_pos.x += 1.5f;
                }
				
				// plus or minus
				hit = Physics2D.Raycast(current_pos, current_direction, Mathf.Infinity, ground_mask | object_mask | portal_mask | movingGround_mask);
			}
			else
			{
				Debug.DrawLine(current_pos, hit.point, Color.red);
				point_list[point_list.Count-1].Add(hit.point);
				
				break;
			}
		}


        int done_path_count = 0;


        foreach (var plist in point_list)
        {
            for (int i = 1; i < plist.Count; i++)
            {
                if (done_path_count >= GetComponentInChildren<Transform>().childCount)
                {
                    GameObject clone = Instantiate(light_prefab, new Vector3(plist[i].x, plist[i].y),
                                                    Quaternion.identity) as GameObject;
                    ParticleSystem ps = clone.transform.GetChild(0).GetComponent<ParticleSystem>();
                    Color color = new Color(laser_color == ELightColor.RED ? 1f : 0f,
                                            laser_color == ELightColor.GREEN ? 1f : 0f,
                                            laser_color == ELightColor.BLUE ? 1f : 0f);
                    ps.startColor = color;

                    clone.transform.position = new Vector3((plist[i - 1].x + plist[i].x) / 2, (plist[i - 1].y + plist[i].y) / 2, 0.5f);
                    float angle = Vector2.Angle(plist[i] - plist[i - 1], Vector2.right);
                    Vector3 cross = Vector3.Cross(plist[i] - plist[i - 1], Vector2.right);
                    if (cross.z > 0)
                        clone.transform.eulerAngles = new Vector3(0, 0, 360.0f - angle);
                    else
                        clone.transform.eulerAngles = new Vector3(0, 0, angle);

                    float distance = Vector2.Distance(plist[i - 1], plist[i]);
                    clone.transform.localScale = new Vector3(distance / light_prefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x,
                                                             0.1125004f, 1.0f);
                    //Debug.Log(distance / light_prefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x);
                    ParticleSystem particle = clone.GetComponentInChildren<ParticleSystem>();
                    particle.startLifetime = distance / particle.startSpeed;
                    Light[] lights = particle.GetComponentsInChildren<Light>();

                    clone.transform.parent = this.transform;
                }
                else
                {
                    Transform child = GetComponentInChildren<Transform>().GetChild(done_path_count);
                    child.transform.position =
                        new Vector3((plist[i - 1].x + plist[i].x) / 2, (plist[i - 1].y + plist[i].y) / 2, 0.5f);
                    float angle = Vector2.Angle(plist[i] - plist[i - 1], Vector2.right);
                    Vector3 cross = Vector3.Cross(plist[i] - plist[i - 1], Vector2.right);

                    float distance = Vector2.Distance(plist[i - 1], plist[i]);
                    ParticleSystem particle = child.GetComponentInChildren<ParticleSystem>();
					particle.startLifetime = distance / particle.startSpeed;
                    if (cross.z > 0)
                        child.transform.eulerAngles = new Vector3(0, 0, 360.0f - angle);
                    else
                        child.transform.eulerAngles = new Vector3(0, 0, angle);

                    child.transform.localScale = new Vector3(Vector2.Distance(plist[i - 1], plist[i]) / light_prefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x,
                                                             0.1125004f, 1.0f);
                }

                done_path_count++;
            }

        }

        for (int i = done_path_count; i < GetComponentInChildren<Transform>().childCount; i++)
        {
            Destroy(GetComponentInChildren<Transform>().GetChild(i).gameObject);
        }
		
	}
}
