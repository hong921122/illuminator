﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class ill_door : MonoBehaviour, IObjectTrigger
{
    public EDirection direction;

    private bool blocking = true;
    public bool Blocking
    {
        set
        {
            if(blocking && !value)
            {
                animation["anim"].speed = 1f;
                animation.Play("anim");
            }
            else if(!blocking && value)
            {
                animation["anim"].speed = -10f;
                animation.Play("anim");
            }

            blocking = value;
        }
        get
        {
            return blocking;
        }
    }

    // Use this for initialization
    void Start()
    {
//		Physics2D.IgnoreLayerCollision(18, 12);
        AnimationCurve curve_x = null;
        AnimationCurve curve_y = null;
        AnimationCurve curve_z = null;

        switch(direction)
        {
            case EDirection.RIGHT:
				curve_x = AnimationCurve.EaseInOut(0f, this.transform.localPosition.x, 1f, this.transform.localPosition.x + 5f);
				curve_y = AnimationCurve.Linear(0f, this.transform.localPosition.y, 1f, this.transform.localPosition.y);
				curve_z = AnimationCurve.Linear(0f, this.transform.localPosition.z, 1f, this.transform.localPosition.z);
                break;

            case EDirection.LEFT:
                curve_x = AnimationCurve.EaseInOut(0f, this.transform.localPosition.x, 1f, this.transform.localPosition.x - 5f);
                curve_y = AnimationCurve.Linear(0f, this.transform.localPosition.y, 1f, this.transform.localPosition.y);
                curve_z = AnimationCurve.Linear(0f, this.transform.localPosition.z, 1f, this.transform.localPosition.z);
                break;

            case EDirection.UP:
                curve_x = AnimationCurve.Linear(0f, this.transform.localPosition.x, 1f, this.transform.localPosition.x);
                curve_y = AnimationCurve.EaseInOut(0f, this.transform.localPosition.y, 1f, this.transform.localPosition.y + 5f);
                curve_z = AnimationCurve.Linear(0f, this.transform.localPosition.z, 1f, this.transform.localPosition.z);
                break;

            case EDirection.DOWN:
                curve_x = AnimationCurve.Linear(0f, this.transform.localPosition.x, 1f, this.transform.localPosition.x);
                curve_y = AnimationCurve.EaseInOut(0f, this.transform.localPosition.y, 1f, this.transform.localPosition.y - 5f);
				curve_z = AnimationCurve.Linear(0f, this.transform.localPosition.z, 1f, this.transform.localPosition.z);
                break;
        }



        AnimationClip clip = new AnimationClip();
        clip.SetCurve("", typeof(Transform), "localPosition.x", curve_x);
        clip.SetCurve("", typeof(Transform), "localPosition.y", curve_y);
        clip.SetCurve("", typeof(Transform), "localPosition.z", curve_z);
        animation.AddClip(clip, "anim");
        animation.wrapMode = WrapMode.Once;
    }

    public void TriggerState()
    {
        Blocking = !Blocking;
    }
}
