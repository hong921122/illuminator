﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ill_spotlight : MonoBehaviour, IObjectInteraction
{

    public LayerMask ground_mask;
    public LayerMask portal_mask;
    public LayerMask object_mask;
    public GameObject light_prefab;

    private Vector2 light_direction;

    [HideInInspector]
    public List<Vector2> point_list;

    private Transform spotlight_holder;

    private Vector3 screenPos;
    private float angleOffset;

    private int curr_start_index;

    private ill_character character;

	public bool lightOn = true;
	public bool canMove = true;

    // Use this for initialization
    void Start()
    {
        character = GameManager.character;

        spotlight_holder = transform.parent.GetChild(1);
        curr_start_index = 0;
    }

    // Update is called once per frame
    void Update()
    {
		if (lightOn) {
			DrawBeam ();

			if (Input.GetKeyDown (UserOptionManager.instance.USE_OBJECT)) {
				if (Vector2.Distance (character.collider2D.bounds.center, this.transform.position) < 5) {
					if (point_list.Count >= 2) {
						//character.transform.position = new Vector2(this.point_list[0].x, this.point_list[0].y); 
						//Debug.Log(this.point_list[0].y);
						curr_start_index = 0;
						character.interacting_type = EInteractWith.SPOTLIGHT;
						character.interacting_object = this.gameObject;
					}
				}
			}
		}
    }

    void DrawBeam()
    {
        // draw ray including reflected
        light_direction = new Vector2(Mathf.Cos(transform.eulerAngles.z * Mathf.Deg2Rad),
                                       Mathf.Sin(transform.eulerAngles.z * Mathf.Deg2Rad));
        RaycastHit2D hit;
        Vector3 current_pos = this.transform.position;
        Vector3 current_direction = light_direction;

        collider2D.enabled = false;
        spotlight_holder.collider2D.enabled = false;
        hit = Physics2D.Raycast(current_pos, current_direction, Mathf.Infinity, ground_mask | object_mask | portal_mask);
        collider2D.enabled = true;
        spotlight_holder.collider2D.enabled = true;

        // points for rendering
        point_list = new List<Vector2>();
        point_list.Add(current_pos);

        // get reflected points
        while (hit.collider != null)
        {
            if (hit.collider.tag == "mirror")
            {
                Debug.DrawLine(current_pos, hit.point, Color.red);
                point_list.Add(hit.point);

                ill_mirror mirror = hit.collider.gameObject.GetComponent<ill_mirror>();

                current_pos = hit.point + mirror.mirror_normal * 0.01f;
                current_direction = mirror.reflect(current_direction);

                // plus or minus
                hit = Physics2D.Raycast(current_pos, current_direction, Mathf.Infinity, ground_mask | object_mask | portal_mask);
            }
            else
            {
                Debug.DrawLine(current_pos, hit.point, Color.red);
                point_list.Add(hit.point);

                break;
            }
        }

        for (int i = 1; i < point_list.Count; i++)
        {
            if (i - 1 >= GetComponentInChildren<Transform>().childCount)
            {
                GameObject clone = Instantiate(light_prefab, new Vector3(point_list[i].x, point_list[i].y),
                                                Quaternion.identity) as GameObject;
                clone.transform.position = new Vector3((point_list[i - 1].x + point_list[i].x) / 2, (point_list[i - 1].y + point_list[i].y) / 2, 0.5f);
                float angle = Vector2.Angle(point_list[i] - point_list[i - 1], Vector2.right);
                Vector3 cross = Vector3.Cross(point_list[i] - point_list[i - 1], Vector2.right);
                if (cross.z > 0)
                    clone.transform.eulerAngles = new Vector3(0, 0, 360.0f - angle);
                else
                    clone.transform.eulerAngles = new Vector3(0, 0, angle);

                float distance = Vector2.Distance(point_list[i - 1], point_list[i]);
                //Debug.Log(Vector2.Distance(point_list[i - 1], point_list[i]));
                clone.transform.localScale = new Vector3(distance / light_prefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x,
                                                         0.1125004f, 1.0f);
                //Debug.Log(distance / light_prefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x);
                ParticleSystem particle = clone.GetComponentInChildren<ParticleSystem>();
                particle.startLifetime = distance / particle.startSpeed;
				Light[] lights = particle.GetComponentsInChildren<Light>();
                //Debug.Log(point_list[i - 1]);
                //Debug.Log(point_list[i]);
                //Debug.Log(distance);
                clone.transform.parent = this.transform;
            }
            else
            {
                Transform child = GetComponentInChildren<Transform>().GetChild(i - 1);
                child.transform.position =
                    new Vector3((point_list[i - 1].x + point_list[i].x) / 2, (point_list[i - 1].y + point_list[i].y) / 2, 0.5f);
                float angle = Vector2.Angle(point_list[i] - point_list[i - 1], Vector2.right);
                Vector3 cross = Vector3.Cross(point_list[i] - point_list[i - 1], Vector2.right);

				float distance = Vector2.Distance(point_list[i - 1], point_list[i]);
				ParticleSystem particle = child.GetComponentInChildren<ParticleSystem>();
				particle.startLifetime = distance / particle.startSpeed;
				if (cross.z > 0)
					child.transform.eulerAngles = new Vector3(0, 0, 360.0f - angle);
                else
                    child.transform.eulerAngles = new Vector3(0, 0, angle);

                child.transform.localScale = new Vector3(Vector2.Distance(point_list[i - 1], point_list[i]) / light_prefab.GetComponent<SpriteRenderer>().sprite.bounds.size.x,
                                                         0.1125004f, 1.0f);
            }
        }

        for (int i = point_list.Count - 1; i < GetComponentInChildren<Transform>().childCount; i++)
        {
            Destroy(GetComponentInChildren<Transform>().GetChild(i).gameObject);
        }
    }

    public void InteractionObject(ill_lightsphere sphere, ill_character character)
    {
        Vector2 character_center = character.getColliderRect().center;

        // spotlight를 타는 도중에
        // spotlight나 이미 거친 mirror의 각도를 변경했을 때
        // out of index exception 방지
        if (curr_start_index >= point_list.Count)
        {
            clearInteraction();
            return;
        }

        character.rigidbody2D.gravityScale = -0.1f;
        character.characterCanMove = false;

        for (int i = 0; i < point_list.Count; i++)
        {
            if (i != point_list.Count - 1)
            {
                if (Vector2.Distance(point_list[i], character_center) < 1.5f)
                {
                    curr_start_index = i;
                    break;
                }
            }
            else
            {
                if (Vector2.Distance(point_list[i], character_center) < 6.0f)
                {
                    // when reach last point
                    clearInteraction();
                    return;
                }
            }
        }


        Vector2 light_force = (point_list[curr_start_index + 1] - new Vector2(character_center.x, character_center.y)).normalized;
        light_force *= 500;
        character.rigidbody2D.AddForce(light_force);

        Debug.DrawLine(character_center, character_center + light_force);

        if (character.rigidbody2D.velocity.magnitude > 5.0f)
        {
            character.rigidbody2D.velocity = (character.rigidbody2D.velocity.normalized) * 5;
        }
    }

    private void clearInteraction()
    {
        character.interacting_type = EInteractWith.NORMAL;
        character.interacting_object = null;
        character.rigidbody2D.gravityScale = 2;
        character.characterCanMove = true;
    }

    void OnMouseDrag()
    {
		if (canMove) {
			Vector3 v3 = Input.mousePosition - screenPos;
			float angle = Mathf.Atan2 (v3.y, v3.x) * Mathf.Rad2Deg;
			if (angle + angleOffset >= -20 && angle + angleOffset < 200)
				transform.eulerAngles = new Vector3 (0, 0, angle + angleOffset);
		}
    }

    void OnMouseDown()
    {
        screenPos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 v3 = Input.mousePosition - screenPos;
        angleOffset = (Mathf.Atan2(transform.right.y, transform.right.x) - Mathf.Atan2(v3.y, v3.x)) * Mathf.Rad2Deg;
    }
}