﻿using UnityEngine;
using System.Collections;

public class EditorManager : MonoBehaviour
{
    private float speed = 0.7f;
    private Vector2 scroll_position = Vector2.zero;
    private Texture2D[] object_sprites;
    private GameObject[] object_prefabs;
    private bool gui_flag = false;
    private Vector3 prev_mouse_pos;

    [HideInInspector]
    public bool object_drag_flag = false;

    private bool isPanning;		// Is the camera being panned?

    // Use this for initialization
    void Start()
    {
        isPanning = false;
        object_sprites = Resources.LoadAll<Texture2D>("game_object/thumbnail");
        object_prefabs = Resources.LoadAll<GameObject>("object_prefab");
    }

    // Update is called once per frame
    void Update()
    {
        if (!gui_flag && !object_drag_flag)
        {
            Debug.Log("testQQQQQQQQ");
            if (Input.GetMouseButtonDown(0) || isPanning)
            {
                if (isPanning)
                {
                    Vector3 pos = Camera.main.ScreenToWorldPoint(AbsoluteVector(Input.mousePosition)) - Camera.main.ScreenToWorldPoint(AbsoluteVector(prev_mouse_pos));
                    Vector3 move = new Vector3(transform.position.x + pos.x * speed * -1, transform.position.y + pos.y * speed * -1, 0);
                    Debug.Log("pos" + pos.ToString());
                    Debug.Log("move" + move.ToString());
                    Camera.main.transform.position = new Vector3(move.x, move.y, -10);
                }
                prev_mouse_pos = Input.mousePosition;

                isPanning = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                isPanning = false;
                prev_mouse_pos = Vector3.zero;
            }
            if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("Waaaa");
            }
            if (Input.GetMouseButtonDown(2))
            {
                Debug.Log("qqqqqqqW");
            }
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize - 1, 10);
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize + 1, 10);
            }
        }
    }

    void OnGUI()
    {
        Rect back_btn = new Rect(10, 10, 100, 50);
        Rect save_map_btn = new Rect(150, 10, 100, 50);
        Rect obj_list = new Rect(Screen.width * 0.85f, Screen.height * 0.2f, Screen.width * 0.15f, Screen.height * 0.8f);

        if (back_btn.Contains(Event.current.mousePosition) || save_map_btn.Contains(Event.current.mousePosition) || obj_list.Contains(Event.current.mousePosition))
        {
            gui_flag = true;
        }
        else
        {
            gui_flag = false;
        }

        GUI.Button(back_btn, "Back");
        GUI.Button(save_map_btn, "Save Map");

        scroll_position = GUI.BeginScrollView(obj_list, scroll_position, new Rect(0, 0, Screen.width * 0.1f, 1200), false, true);

        Rect hang_lamp = new Rect(0, 0, 100, 150);
        Rect street_lamp = new Rect(0, 150, 100, 150);
        Rect mirror = new Rect(0, 300, 100, 150);
        Rect portal = new Rect(0, 450, 100, 150);
        Rect wind = new Rect(0, 600, 100, 150);
        Rect save_point = new Rect(0, 750, 100, 150);
        Rect spotlight = new Rect(0, 900, 100, 150);
        Rect trigger = new Rect(0, 1050, 100, 150);
        //Rect waterfall 		= new Rect (0, 1200, 100, 150);

        if (GUI.Button(hang_lamp, "", new GUIStyle() { normal = new GUIStyleState() { background = object_sprites[2] } }))
        {
            GameObject clone = Instantiate(object_prefabs[8], Vector3.zero, Quaternion.identity) as GameObject;
            clone.AddComponent("EditorObject");
        }
        if (GUI.Button(mirror, "", new GUIStyle() { normal = new GUIStyleState() { background = object_sprites[3] } }))
        {
            GameObject clone = Instantiate(object_prefabs[10], Vector3.zero, Quaternion.identity) as GameObject;
            clone.AddComponent("EditorObject");
        }
        if (GUI.Button(portal, "", new GUIStyle() { normal = new GUIStyleState() { background = object_sprites[4] } }))
        {
            GameObject clone = Instantiate(object_prefabs[11], Vector3.zero, Quaternion.identity) as GameObject;
            clone.AddComponent("EditorObject");
        }
        if (GUI.Button(save_point, "", new GUIStyle() { normal = new GUIStyleState() { background = object_sprites[5] } }))
        {
            GameObject clone = Instantiate(object_prefabs[14], Vector3.zero, Quaternion.identity) as GameObject;
            clone.AddComponent("EditorObject");
        }
        if (GUI.Button(spotlight, "", new GUIStyle() { normal = new GUIStyleState() { background = object_sprites[6] } }))
        {
            GameObject clone = Instantiate(object_prefabs[16], Vector3.zero, Quaternion.identity) as GameObject;
            clone.AddComponent("EditorObject");
        }
        if (GUI.Button(street_lamp, "", new GUIStyle() { normal = new GUIStyleState() { background = object_sprites[7] } }))
        {
            GameObject clone = Instantiate(object_prefabs[17], Vector3.zero, Quaternion.identity) as GameObject;
            clone.AddComponent("EditorObject");
        }
        if (GUI.Button(trigger, "", new GUIStyle() { normal = new GUIStyleState() { background = object_sprites[8] } }))
        {
            GameObject clone = Instantiate(object_prefabs[18], Vector3.zero, Quaternion.identity) as GameObject;
            clone.AddComponent("EditorObject");
        }
        if (GUI.Button(wind, "", new GUIStyle() { normal = new GUIStyleState() { background = object_sprites[9] } }))
        {
            GameObject clone = Instantiate(object_prefabs[13], Vector3.zero, Quaternion.identity) as GameObject;
            clone.AddComponent("EditorObject");
        }


        //		GUI.DrawTexture (waterfall, object_sprites [3]);


        //		GUI.Label(new Rect(0, 0, 200, 30), "UP", label_style);
        //		GUI.Label(new Rect(0, 40, 200, 30), "DOWN", label_style);
        //		GUI.Label(new Rect(0, 80, 200, 30), "LEFT", label_style);
        //		GUI.Label(new Rect(0, 120, 200, 30), "RIGHT", label_style);
        //		GUI.Label(new Rect(0, 160, 200, 30), "CHANGE COLOR 1", label_style);
        //		GUI.Label(new Rect(0, 200, 200, 30), "CHANGE COLOR 2", label_style);
        //		GUI.Label(new Rect(0, 240, 200, 30), "CHANGE COLOR 3", label_style);
        //		GUI.Label(new Rect(0, 280, 200, 30), "CHANGE COLOR 4", label_style);
        //		GUI.Label(new Rect(0, 320, 200, 30), "USE OBJECT", label_style);
        //		GUI.Label(new Rect(0, 360, 200, 30), "STOP OBJECT", label_style);
        //		GUI.Label(new Rect(0, 400, 200, 30), "JUMP", label_style);
        //
        //		Rect button_up = new Rect(300, 0, 100, 30);
        //		Rect button_down = new Rect(300, 40, 100, 30);
        //		Rect button_left = new Rect(300, 80, 100, 30);
        //		Rect button_right = new Rect(300, 120, 100, 30);
        //		Rect button_color1 = new Rect(300, 160, 100, 30);
        //		Rect button_color2 = new Rect(300, 200, 100, 30);
        //		Rect button_color3 = new Rect(300, 240, 100, 30);
        //		Rect button_color4 = new Rect(300, 280, 100, 30);
        //		Rect button_use = new Rect(300, 320, 100, 30);
        //		Rect button_stop = new Rect(300, 360, 100, 30);
        //		Rect button_jump = new Rect(300, 400, 100, 30);
        GUI.EndScrollView();
    }

    void OnMouseDown()
    {
        Debug.Log("TQ");
    }
    void OnMouseDrag()
    {
        Debug.Log("T");
        while (Input.GetMouseButtonDown(1))
        {
            //			float v = vertical_speed * Input.GetAxis("Mouse X");
            //			float h = horizontal_speed * Input.GetAxis("Mouse Y");
            //			transform.Translate(v, h, 0);
        }
    }

    private Vector3 AbsoluteVector(Vector3 pos)
    {
        return new Vector3(Mathf.Abs(pos.x),
                            Mathf.Abs(pos.y),
                            Mathf.Abs(pos.z));
    }
}
