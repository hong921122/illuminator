﻿using UnityEngine;
using System.Collections;

public class EditorObject : MonoBehaviour
{
    public bool raycast_flag;
    public bool drag_flag;
    public Vector3 prev_mousepos;

    private float speed = 0.7f;
    private Vector3 prev_mouse_pos;

    // Use this for initialization
    void Start()
    {
        raycast_flag = false;
        drag_flag = false;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(transform.name);
        Debug.Log(Camera.main.WorldToScreenPoint(Vector3.zero));
        if ((Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero)).collider != null)
        {
            raycast_flag = true;
        }
        else
        {
            raycast_flag = false;
        }

        if ((Input.GetMouseButtonDown(0) && raycast_flag) || drag_flag)
        {
            if (drag_flag)
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(AbsoluteVector(Input.mousePosition)) - Camera.main.ScreenToWorldPoint(AbsoluteVector(prev_mouse_pos));
                Vector3 move = new Vector3(transform.position.x + pos.x * speed, transform.position.y + pos.y * speed, 0);
                transform.position = new Vector3(move.x, move.y, 0);
                Camera.main.GetComponent<EditorManager>().object_drag_flag = true;
            }
            prev_mouse_pos = Input.mousePosition;

            drag_flag = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            drag_flag = false;
            prev_mouse_pos = Vector3.zero;
            Camera.main.GetComponent<EditorManager>().object_drag_flag = false;
        }

        //		Debug.Log (raycast_flag);
        //		Debug.Log (drag_flag);

        //
        //		if ((Input.GetMouseButtonDown (0) && raycast_flag) || drag_flag) {
        //			drag_flag = true;
        //			Debug.Log("AAJAJAJAJAJAJAJAJ");
        //			transform.position = new Vector3(transform.position.x + Camera.main.ScreenToWorldPoint(Input.mousePosition).x * 0.01f,
        //			                                 transform.position.y + Camera.main.ScreenToWorldPoint(Input.mousePosition).y * 0.01f,
        //			                                 transform.position.z
        //			                                 );
        //			Camera.main.GetComponent<EditorManager> ().object_drag_flag = true;
        //		}
        //
        //		if (Input.GetMouseButtonUp (0)) {
        //			drag_flag = false;
        //			raycast_flag = false;
        //			Camera.main.GetComponent<EditorManager> ().object_drag_flag = false;
        //		}
        //		prev_mousepos = Input.mousePosition;





        //		foreach (SpriteRenderer a in gameObject.GetComponentsInChildren<SpriteRenderer>()) {
        //			Debug.Log (a.bounds.size);
        //			Debug.Log (Camera.main.ScreenToWorldPoint(Input.mousePosition));
        //			Debug.Log (a.bounds.Contains(
        //				Camera.main.ScreenToWorldPoint(Input.mousePosition)));
        //		}
    }

    private Vector3 AbsoluteVector(Vector3 pos)
    {
        return new Vector3(Mathf.Abs(pos.x),
                           Mathf.Abs(pos.y),
                           Mathf.Abs(pos.z));
    }
}
