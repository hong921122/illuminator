﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SoundEffectManager : MonoBehaviour {
    private AudioSource audiosource;

    public AudioClip clip_jump;
    public AudioClip clip_attach_sphere;
    public AudioClip clip_trigger;
    public AudioClip clip_portal;
    public AudioClip clip_energy_disapear;
    public AudioClip clip_lens_bigger;
    public AudioClip clip_lens_smaller;

    private static SoundEffectManager _instance;
    public static SoundEffectManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SoundEffectManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

	// Use this for initialization
	void Start () {
        audiosource = this.GetComponent<AudioSource>();
	}

    public void play(AudioClip clip)
    {
        audiosource.PlayOneShot(clip, UserOptionManager.instance.VOLUME_EFFECT);
    }
}
