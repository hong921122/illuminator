﻿using UnityEngine;
using System.Collections;

public class FadeScene : MonoBehaviour
{
    public Texture2D fadeout_white_texture;
    public Texture2D fadeout_texture;
    public float fade_speed = 0.8f;

    public int draw_depth = -1000;
    public float alpha = 1.0f;
    public int fade_direction = -1;
    public bool color_type;

    void OnGUI()
    {
        alpha += fade_direction * fade_speed * Time.deltaTime;
        alpha = Mathf.Clamp01(alpha);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.depth = draw_depth;
        if (!color_type)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeout_white_texture);
        else
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeout_texture);
    }

    public float BeginFade(int direction, bool color)
    {
        color_type = color;
        fade_direction = direction;
        return (fade_speed);
    }

    void OnLevelWasLoaded()
    {
        BeginFade(-1, color_type);
    }
}
