﻿using UnityEngine;
using System.Collections;

public class OptionMouseOver : MonoBehaviour
{
    OptionSceneController option_controller;

    void Start()
    {
        option_controller = GameObject.Find("Main Camera").GetComponent<OptionSceneController>();
    }

    void Update()
    {
        if (option_controller.current_view == this.gameObject.name)
        {
            this.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
        }
        else
        {
            //this.GetComponent<SpriteRenderer> ().color = new Color (255, 255, 255);
        }
    }

    void OnMouseEnter()
    {
        SpriteRenderer sprite = this.GetComponent<SpriteRenderer>();
        sprite.color = new Color(255, 0, 0);
    }

    void OnMouseExit()
    {
        SpriteRenderer sprite = this.GetComponent<SpriteRenderer>();
        sprite.color = new Color(255, 255, 255);
    }

    void OnMouseDown()
    {
        if (gameObject.name == "keyboard")
        {
            GameObject.Find(option_controller.current_view).GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
            option_controller.current_view = "keyboard";
        }
        if (gameObject.name == "mouse_sensitivity")
        {
            GameObject.Find(option_controller.current_view).GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
            option_controller.current_view = "mouse_sensitivity";
        }
        if (gameObject.name == "back")
        {
            StartCoroutine("SelectMenu", "initial_scene");
        }
        if (gameObject.name == "sound")
        {
            GameObject.Find(option_controller.current_view).GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
            option_controller.current_view = "sound";
        }
    }

    private IEnumerator SelectMenu(string level)
    {
        yield return new WaitForSeconds(0.6f);
        float fade_time = GameObject.Find("Main Camera").GetComponent<FadeScene>().BeginFade(1, true);
        yield return new WaitForSeconds(fade_time);
        Application.LoadLevel(level);
    }
}
