using UnityEngine;
using System.Collections;

public class OptionSceneController : MonoBehaviour
{
    public string current_view;
    public Texture2D background_texture;
    public Texture2D thumb_texture;
    public Texture2D sound_texture;
    public Texture2D sound_effect_texture;
    public Vector2 scroll_position = Vector2.zero;
    public Vector2 click_button;
    public float slider_value;
    public float slider_sound_bgm_value;
    public float slider_sound_effect_value;
    public GUIStyle label_style;
    public GUIStyle subject_style;
    public GUIStyle scrollview_style;

    // Use this for initialization
    void Start()
    {
        Transform tr = GameObject.Find("New Sprite").transform;
        SpriteRenderer sr = GameObject.Find("New Sprite").GetComponent<SpriteRenderer>();

        tr.transform.localScale = new Vector3(1, 1, 1);

        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;

        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        tr.transform.localScale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, tr.transform.localScale.z);

        current_view = "keyboard";
        slider_sound_bgm_value = UserOptionManager.instance.VOLUME_BGM;
        slider_sound_effect_value = UserOptionManager.instance.VOLUME_EFFECT;
        GameObject.Find("background_image").GetComponent<GUITexture>().pixelInset = new Rect(Screen.width / 2, Screen.height / 2, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mouse_pos = Input.mousePosition;
        mouse_pos.z = 3.0f;
        GameObject.Find("Point light").transform.position = Camera.main.ScreenToWorldPoint(mouse_pos);
    }

    void OnGUI()
    {
        GUI.Label(new Rect((Screen.width - 200) / 2, Screen.height * 0.15f, 200, 30), "OPTION", subject_style);
        if (current_view == "keyboard")
        {
            GUI.skin.scrollView = scrollview_style;
            scroll_position = GUI.BeginScrollView(new Rect(Screen.width * 0.4f, Screen.height * 0.4f, Screen.width * 0.4f, Screen.height * 0.5f), scroll_position, new Rect(0, 0, 400, 500), false, true);


            GUI.Label(new Rect(0, 0, 200, 30), "UP", label_style);
            GUI.Label(new Rect(0, 40, 200, 30), "DOWN", label_style);
            GUI.Label(new Rect(0, 80, 200, 30), "LEFT", label_style);
            GUI.Label(new Rect(0, 120, 200, 30), "RIGHT", label_style);
            GUI.Label(new Rect(0, 160, 200, 30), "CHANGE COLOR 1", label_style);
            GUI.Label(new Rect(0, 200, 200, 30), "CHANGE COLOR 2", label_style);
            GUI.Label(new Rect(0, 240, 200, 30), "CHANGE COLOR 3", label_style);
            GUI.Label(new Rect(0, 280, 200, 30), "CHANGE COLOR 4", label_style);
            GUI.Label(new Rect(0, 320, 200, 30), "USE OBJECT", label_style);
            GUI.Label(new Rect(0, 360, 200, 30), "STOP OBJECT", label_style);
            GUI.Label(new Rect(0, 400, 200, 30), "JUMP", label_style);

            Rect button_up = new Rect(300, 0, 100, 30);
            Rect button_down = new Rect(300, 40, 100, 30);
            Rect button_left = new Rect(300, 80, 100, 30);
            Rect button_right = new Rect(300, 120, 100, 30);
            Rect button_color1 = new Rect(300, 160, 100, 30);
            Rect button_color2 = new Rect(300, 200, 100, 30);
            Rect button_color3 = new Rect(300, 240, 100, 30);
            Rect button_color4 = new Rect(300, 280, 100, 30);
            Rect button_use = new Rect(300, 320, 100, 30);
            Rect button_stop = new Rect(300, 360, 100, 30);
            Rect button_jump = new Rect(300, 400, 100, 30);

            if (GUI.Button(button_up, UserOptionManager.instance.MOVE_UP.ToString()) || button_up.Contains(click_button))
            {
                click_button = button_up.center;
                GUI.Button(button_up, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.MOVE_UP = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_down, UserOptionManager.instance.MOVE_DOWN.ToString()) || button_down.Contains(click_button))
            {
                click_button = button_down.center;
                GUI.Button(button_down, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.MOVE_DOWN = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_left, UserOptionManager.instance.MOVE_LEFT.ToString()) || button_left.Contains(click_button))
            {
                click_button = button_left.center;
                GUI.Button(button_left, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.MOVE_LEFT = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_right, UserOptionManager.instance.MOVE_RIGHT.ToString()) || button_right.Contains(click_button))
            {
                click_button = button_right.center;
                GUI.Button(button_right, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.MOVE_RIGHT = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_color1, UserOptionManager.instance.SPHERE_COLOR_TYPE_1.ToString()) || button_color1.Contains(click_button))
            {
                click_button = button_color1.center;
                GUI.Button(button_color1, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.SPHERE_COLOR_TYPE_1 = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_color2, UserOptionManager.instance.SPHERE_COLOR_TYPE_2.ToString()) || button_color2.Contains(click_button))
            {
                click_button = button_color2.center;
                GUI.Button(button_color2, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.SPHERE_COLOR_TYPE_2 = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_color3, UserOptionManager.instance.SPHERE_COLOR_TYPE_3.ToString()) || button_color3.Contains(click_button))
            {
                click_button = button_color3.center;
                GUI.Button(button_color3, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.SPHERE_COLOR_TYPE_3 = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_color4, UserOptionManager.instance.SPHERE_COLOR_TYPE_4.ToString()) || button_color4.Contains(click_button))
            {
                click_button = button_color4.center;
                GUI.Button(button_color4, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.SPHERE_COLOR_TYPE_4 = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_use, UserOptionManager.instance.USE_OBJECT.ToString()) || button_use.Contains(click_button))
            {
                click_button = button_use.center;
                GUI.Button(button_use, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.USE_OBJECT = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_stop, UserOptionManager.instance.STOP_OBJECT.ToString()) || button_stop.Contains(click_button))
            {
                click_button = button_stop.center;
                GUI.Button(button_stop, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.STOP_OBJECT = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }

            if (GUI.Button(button_jump, UserOptionManager.instance.JUMP.ToString()) || button_jump.Contains(click_button))
            {
                click_button = button_jump.center;
                GUI.Button(button_jump, "");

                if (Event.current.isKey)
                {
                    UserOptionManager.instance.JUMP = Event.current.keyCode;
                    click_button = new Vector2();
                }
            }
            GUI.EndScrollView();
        }

        if (current_view == "mouse_sensitivity")
        {
            GUIStyle style = new GUIStyle();
            GUIStyle thumb = new GUIStyle();
            style.normal.background = background_texture;
            style.fixedHeight = 20;
            style.fixedWidth = Screen.width * 0.4f;
            thumb.normal.background = thumb_texture;
            thumb.fixedWidth = 20;
            thumb.fixedHeight = 20;
            slider_value = GUI.HorizontalSlider(new Rect(Screen.width * 0.3f, Screen.height * 0.4f, Screen.width * 0.4f, 200), slider_value, 0, 100, style, thumb);
            UserOptionManager.instance.MOUSE_SENSITIVITY = slider_value;
        }

        if (current_view == "sound")
        {
            GUI.DrawTexture(new Rect(Screen.width * 0.26f, Screen.height * 0.35f, sound_texture.width / 2, sound_texture.height / 2), sound_texture);
            GUIStyle style = new GUIStyle();
            GUIStyle thumb = new GUIStyle();
            style.normal.background = background_texture;
            style.fixedHeight = 20;
            style.fixedWidth = Screen.width * 0.4f;
            thumb.normal.background = thumb_texture;
            thumb.fixedWidth = 20;
            thumb.fixedHeight = 20;
            slider_sound_bgm_value = GUI.HorizontalSlider(new Rect(Screen.width * 0.3f, Screen.height * 0.45f, Screen.width * 0.4f, 200), slider_sound_bgm_value, 0, 1, style, thumb);

            GUI.DrawTexture(new Rect(Screen.width * 0.26f, Screen.height * 0.6f, sound_texture.width / 2, sound_texture.height / 2), sound_effect_texture);
            slider_sound_effect_value = GUI.HorizontalSlider(new Rect(Screen.width * 0.3f, Screen.height * 0.7f, Screen.width * 0.4f, 200), slider_sound_effect_value, 0, 1, style, thumb);
            UserOptionManager.instance.VOLUME_BGM = slider_sound_bgm_value;
            UserOptionManager.instance.VOLUME_EFFECT = slider_sound_effect_value;
        }
    }
}
