using UnityEngine;
using System.Collections;

public class InitialSceneController : MonoBehaviour
{
    public Texture newGame_btn;
    public Texture2D loadGame_btn;
    public Texture2D settings_btn;
    public Texture2D exit_btn;
    public Texture2D background_texture;
    public Texture2D background_light;

    public GUIText text1;
    public GUIText text2;
    public GUIText text3;
    public GUIText text4;
    public GUIStyle label_style;
    //	private float light_height;
    //	private bool light_flag;

    private float overlay_width;
    private float overlay_height;
    private Transform overlay;
    public Texture2D cursor_texture;

    void Start()
    {
        //Cursor.SetCursor(cursor_texture, new Vector2(cursor_texture.width * 0.07738099f, cursor_texture.height * 0.9826693f), CursorMode.Auto);
        Transform tr = GameObject.Find("New Sprite").transform;
        SpriteRenderer sr = GameObject.Find("New Sprite").GetComponent<SpriteRenderer>();

        tr.transform.localScale = new Vector3(1, 1, 1);

        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;

        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        tr.transform.localScale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, tr.transform.localScale.z);
    }

    void Update()
    {
        Vector3 mouse_pos = Input.mousePosition;
        mouse_pos.z = 3.0f;
        GameObject.Find("Point light").transform.position = Camera.main.ScreenToWorldPoint(mouse_pos);
    }

    void OnGUI()
    {
        //GUI.Label(new Rect((Screen.width - 200) / 2, Screen.height * 0.15f, 200, 30), "ILLUMINATOR", label_style);
	}

    private IEnumerator SelectMenu(string level)
    {
        yield return new WaitForSeconds(0.6f);
        float fade_time = GameObject.Find("Main Camera").GetComponent<FadeScene>().BeginFade(1, true);
        yield return new WaitForSeconds(fade_time);
        Application.LoadLevel(level);
    }
}
