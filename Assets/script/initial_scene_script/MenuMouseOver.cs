﻿using UnityEngine;
using System.Collections;

public class MenuMouseOver : MonoBehaviour
{
    void OnMouseEnter()
    {
        SpriteRenderer sprite = this.GetComponent<SpriteRenderer>();
        sprite.color = new Color(255, 0, 0);
    }

    void OnMouseExit()
    {
        SpriteRenderer sprite = this.GetComponent<SpriteRenderer>();
        sprite.color = new Color(255, 255, 255);
    }

    void OnMouseDown()
    {
        if (gameObject.name == "new")
        {
            StartCoroutine("SelectMenu", "chapter_1");
            MusicManager.instance.Stop();
        }
        if (gameObject.name == "load")
        {
            SaveLoadGame.LoadGame();
            StartCoroutine("SelectMenu", "chapter_select_scene");
        }
        if (gameObject.name == "help")
        {
        }
        if (gameObject.name == "option")
        {
            StartCoroutine("SelectMenu", "option_scene");
        }
        if (gameObject.name == "exit")
        {
            Application.Quit();
        }
    }

    private IEnumerator SelectMenu(string level)
    {
        yield return new WaitForSeconds(0.6f);
        float fade_time = GameObject.Find("Main Camera").GetComponent<FadeScene>().BeginFade(1, true);
        yield return new WaitForSeconds(fade_time);
        Application.LoadLevel(level);
    }

    private IEnumerator SelectMenuFadeWhite(string level)
    {
        yield return new WaitForSeconds(0.6f);
        GameObject.Find("Main Camera").GetComponent<FadeScene>().fade_speed = 3.0f;
        float fade_time = GameObject.Find("Main Camera").GetComponent<FadeScene>().BeginFade(1, false);
        yield return new WaitForSeconds(fade_time);
        StartCoroutine(gameObject.GetComponent<LoadingProgress>().LoadStageAsync(level));
        //		Application.LoadLevel (level);
    }
}
